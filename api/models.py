from pydantic import BaseModel
from typing import List, Optional
from jwtdown_fastapi.authentication import Token
from datetime import date

today = date.today()

class AccountIn(BaseModel):
    full_name: str
    username: str
    password: str


class AccountOut(BaseModel):
    id: str
    full_name: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class DeleteStatus(BaseModel):
    deleted: bool


class TaskIn(BaseModel):
    task: str
    due_date: str
    completed: str = "to do"
    date_created: str = today
    description: str


class TaskOut(BaseModel):
    id: str
    task: str
    due_date: str
    completed: str
    date_created: str = today
    description: str


class TaskList(BaseModel):
    tasks: List[TaskOut]
