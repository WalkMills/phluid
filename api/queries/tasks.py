from queries.client import MongoQueries
from models import TaskIn
from bson.objectid import ObjectId


class TaskQueries(MongoQueries):
    collection_name = "tasks"

    def create(self, task_in: TaskIn, account_id: str):
        task = task_in.dict()
        task["account_id"] = account_id
        self.collection.insert_one(task)
        task["id"] = str(task["_id"])
        return task


    def get_all(self, account_id: str):
        result = []
        for task in self.collection.find({"account_id": account_id}):
            task["id"] = str(task["_id"])
            result.append(task)
        return result


    def get_one(self, task_id: str):
        task = self.collection.find_one({"_id": ObjectId(task_id)})
        if task is None:
            return None
        task["id"] = str(task["_id"])
        return task


    def update(self, task_in: TaskIn, task_id: str, account_id: str):
        task = task_in.dict()
        task["account_id"] = account_id
        result = self.collection.update_one(
            {"_id": ObjectId(task_id)}, {"$set": task}
        )
        if result.matched_count == 0:
            return None
        task["id"] = task_id
        return task


    def delete(self, task_id: str, account_id: str):
        result = self.collection.delete_one(
            {"_id": ObjectId(task_id), "account_id": account_id}
        )
        return result.deleted_count > 0
