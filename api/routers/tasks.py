from models import TaskIn, TaskOut, TaskList
from fastapi import APIRouter, Depends, HTTPException
from authenticator import authenticator
from queries.tasks import TaskQueries


router = APIRouter()


@router.get("/api/tasks", response_model=TaskList)
def list_task(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TaskQueries = Depends()
):
    return {"tasks": queries.get_all(account_id=account_data["id"])}


@router.post("/api/tasks", response_model=TaskOut)
def create_task(
    task_in: TaskIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TaskQueries = Depends()
):
    return queries.create(task_in=task_in, account_id=account_data["id"])


@router.get("/api/tasks/{task_id}", response_model=TaskOut)
def get_one_task(
    task_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TaskQueries = Depends()
):
    task = queries.get_one(task_id)
    if task is None:
        raise HTTPException(status_code = 404, detail="task not found")
    return task


@router.put("/api/tasks/{task_id}", response_model=TaskOut)
def update_task(
    task_in: TaskIn,
    task_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TaskQueries = Depends()
):
    task = queries.update(
        task_id=task_id,
        task_in=task_in,
        account_id=account_data["id"]
    )
    if task is None:
        raise HTTPException(status_code = 404, detail="task not found")
    return task


@router.delete("/api/tasks/{task_id}")
def delete_task(
    task_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TaskQueries = Depends()
):
    return {
        "deleted": queries.delete(
            task_id=task_id, account_id=account_data["id"]
        )
    }
