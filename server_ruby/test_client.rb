
require "http-client"
require "json"

URL  = "http://localhost:4567"
USER = "jack"

$\ = "\n"

#-----------------------------------------------------------------------------------------------------------------------
########################################################################################################################

grid_test = {

   "empty grid" => [
      "1-15-2023",
      [ ] ],

   "single element grid, after last, adjacent to single" => [
      "2-15-2023",
      [ [ "2-14-2023" ] ] ],

   "single element grid, after last, adjacent to double" => [
      "3-15-2023",
      [ [ "3-12-2023", "3-14-2023" ] ] ],

   "single element grid, before first, adjacent to single" => [
      "4-15-2023",
      [ [ "4-16-2023" ] ] ],

   "single element grid, before first, adjacent to double" => [
      "5-15-2023",
      [ [ "5-16-2023", "5-18-2023" ] ] ],

   "single element grid, after last, non-adjacent to single" => [
      "6-15-2023",
      [ [ "6-12-2023" ] ] ],

   "single element grid, after last, non-adjacent to double" => [
      "7-15-2023",
      [ [ "7-10-2023", "7-12-2023" ] ] ],

   "single element grid, before first, non-adjacent to single" => [
      "8-15-2023",
      [ [ "8-17-2023" ] ] ],

   "single element grid, before first, non-adjacent to double" => [
      "9-15-2023",
      [ [ "9-17-2023", "9-18-2023" ] ] ],

   "after last, adjacent to single" => [
      "10-15-2023",
      [ [ "10-8-2023" ], [ "10-10-2023", "10-12-2023" ], [ "10-14-2023" ] ] ],

   "after last, adjacent to double" => [
      "11-15-2023",
      [ [ "11-8-2023" ], [ "11-10-2023" ], [ "11-12-2023", "11-14-2023" ] ] ],

   "after last, non-adjacent to single" => [
      "12-16-2023",
      [ [ "12-8-2023" ], [ "12-10-2023", "12-12-2023" ], [ "12-14-2023" ] ] ],

   "after last, non-adjacent to double" => [
      "1-16-2024",
      [ [ "1-8-2024" ], [ "1-10-2024" ], [ "1-12-2024", "1-14-2024" ] ] ],

   "before first, adjacent to single" => [
      "2-7-2024",
      [ [ "2-8-2024" ], [ "2-10-2024", "2-12-2024" ], [ "2-14-2024" ] ] ],

   "before first, adjacent to double" => [
      "3-7-2024",
      [ [ "3-8-2024", "3-10-2024" ], [ "3-12-2024" ], [ "3-14-2024" ] ] ],

   "before first, non-adjacent to single" => [
      "4-6-2024",
      [ [ "4-8-2024" ], [ "4-10-2024", "4-12-2024" ], [ "4-14-2024" ] ] ],

   "before first, non-adjacent to double" => [
      "5-6-2024",
      [ [ "5-8-2024", "5-10-2024" ], [ "5-12-2024" ], [ "5-14-2024" ] ] ],

   "between single-single, non-adjacent to both" => [
      "6-12-2024",
      [ [ "6-6-2024", "6-8-2024" ], [ "6-10-2024" ], [ "6-14-2024" ] ] ],

   "between single-double, non-adjacent to both" => [
      "7-14-2024",
      [ [ "7-8-2024" ], [ "7-12-2024" ], [ "7-16-2024", "7-18-2024" ] ] ],

   "between double-single, non-adjacent to both" => [
      "8-14-2024",
      [ [ "8-6-2024" ], [ "8-10-2024", "8-12-2024" ], [ "8-16-2024" ] ] ],

   "between double-double, non-adjacent to both" => [
      "9-14-2024",
      [ [ "9-6-2024" ], [ "9-10-2024", "9-12-2024" ], [ "9-16-2024", "9-18-2024" ] ] ],

   "between single-single, adjacent to earlier" => [
      "10-7-2024",
      [ [ "10-6-2024" ], [ "10-9-2024" ], [ "10-16-2024" ] ] ],

   "between single-double, adjacent to earlier" => [
      "11-7-2024",
      [ [ "11-6-2024" ], [ "11-9-2024", "11-12-2024" ], [ "11-16-2024" ] ] ],

   "between double-single, adjacent to earlier" => [
      "12-7-2024",
      [ [ "12-2-2024", "12-6-2024" ], [ "12-9-2024" ], [ "12-16-2024" ] ] ],

   "between double-double, adjacent to earlier" => [
      "1-7-2025",
      [ [ "1-2-2025", "1-6-2025" ], [ "1-9-2025", "1-12-2025" ], [ "1-16-2025" ] ] ],

   "between single-single, adjacent to later" => [
      "2-8-2025",
      [ [ "2-6-2025" ], [ "2-9-2025" ], [ "2-16-2025" ] ] ],

   "between single-double, adjacent to later" => [
      "3-8-2025",
      [ [ "3-6-2025" ], [ "3-9-2025", "3-12-2025" ], [ "3-16-2025" ] ] ],

   "between double-single, adjacent to later" => [
      "4-8-2025",
      [ [ "4-2-2025", "4-6-2025" ], [ "4-9-2025" ], [ "4-16-2025" ] ] ],

   "between double-double, adjacent to later" => [
      "5-8-2025",
      [ [ "5-2-2025", "5-6-2025" ], [ "5-9-2025", "5-12-2025" ], [ "5-16-2025" ] ] ],

   "between single-single, adjacent to both" => [
      "6-15-2025",
      [ [ "6-2-2025", "6-6-2025" ], [ "6-9-2025", "6-12-2025" ], [ "6-14-2025" ], [ "6-16-2025" ] ] ],

   "between single-double, adjacent to both" => [
      "7-15-2025",
      [ [ "7-2-2025", "7-6-2025" ], [ "7-9-2025", "7-12-2025" ], [ "7-14-2025" ], [ "7-16-2025", "7-18-2025" ] ] ],

   "between double-single, adjacent to both" => [
      "8-15-2025",
      [ [ "8-2-2025", "8-6-2025" ], [ "8-9-2025" ], [ "8-12-2025", "8-14-2025" ], [ "8-16-2025" ] ] ],

   "between double-double, adjacent to both" => [
      "9-15-2025",
      [ [ "9-2-2025", "9-6-2025" ], [ "9-9-2025", "9-10-2025" ], [ "9-12-2025", "9-14-2025" ], [ "9-16-2025", "9-18-2025" ] ] ]
}

########################################################################################################################

endpoint_test = [

   [
      :post,         # method
      [ "/task" ],   # url stack
      [ { } ],       # params stack
      [ {            # body stack
         date: "2023-12-02",
         task_list: [ "101", "808", "404" ],
         new: {
            title: "newly created task",
            type: "text",
            status: "done",
            contents: "contents of newly created task"
         },
         position: 2
      } ]
   ],

   [
      :get,
      [ "/task/101" ],
      [ { } ],
      [ "" ]
   ],

   [
      :put,
      [ "/task/505" ],
      [ { } ],
      [ {
            title: "updated task: leave a shit",
            type: "text",
            status: "done",
            contents: "updated contents of task: leave a shit"
      } ]
   ],

   [
      :put,
      [ "/task/move/404" ],
      [ { } ],
      [ {
         current: "2023-12-04",
         new: "2023-12-03",
         task_list: [ "404", "909", "606" ]
      } ]
   ],

   # [
   #    :delete,
   #    [ "/task/404" ],
   #    [ { date: "2023-12-04" } ],
   #    [ ]
   # ],

   # [
   #    :post,
   #    [ "/date/2023-11-04" ],
   #    [ ],
   #    [ [ ] ]
   # ],

   # [
   #    :get,
   #    [ "/date/2023-12-04" ],
   #    [ ],
   #    [ ]
   # ],

   # [
   #    :get,
   #    [ "/date/2023-12-04" ],
   #    [ { end: "" } ],
   #    [ ]
   # ],

   # [
   #    :put,
   #    [ "/date/2023-12-05" ],
   #    [ ],
   #    [ [ ] ]
   # ],

   # [
   #    :delete,
   #    [ "/date/2023-11-12" ],
   #    [ ],
   #    [ ]
   # ]
]

# all comb/perm of present_good|present_bad|absent for each of the following (applies recursively to any elements

#-----------------------------------------------------------------------------------------------------------------------
separator  = "\n" + "-" * 10
cookie_jar = HTTP::CookieJar.new
login_query = {
   uname: USER,
   pw:    USER
}
response = HTTP::Client.get( URL + "/login", query: login_query, jar: cookie_jar )   # login
print( separator + "\nlogin response status: %s" % response.code )

endpoint_test.each { | method, url_stack, query_stack, body_stack |   # add expected response code
   args = { jar: cookie_jar }
   url_stack.each { | url |
      query_stack.each { | query |
         args[ :query ] = query if ! query.empty?
         body_stack.each { | body |
            args[ :body ] = body.to_json if ! body.empty?
            response = HTTP::Client::Request.new( method, URL + url, args ).execute
            print( separator )
            print( "%s %s" % [ method, URL + url ] )
            print( "response:" )
            print( response.body )
         }
      }
   }
}
