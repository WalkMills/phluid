#!ruby

########################################################################################################################
#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--
#-----------------------------------------------------------------------------------------------------------------------
   #--------------------------------------------------------------------------------------------------------------------

require "redis"
require "json"
require "date"
require "securerandom"

########################################################################################################################

class Populate   # this is for Phluid (using redis) only

   DATE_P = "%Y-%m-%d"   # convert between instances of Date and date key strings
   STATUS = %i[ todo doing done ]

   #--------------------------------------------------------------------------------------------------------------------

   def initialize( redis )

      print( "\n*** flushing then populating database\n" )
      redis.flushall

      users = {    # username must not contain colons; dates must increase non-monotonically
         "walker" => [ [ "2023-11-01" ], [ "2023-11-03", "2023-11-05" ], [ "2023-11-09" ] ],
         "julian" => [ [ "2023-11-09", "2023-11-10" ], [ "2023-11-14", "2023-11-17" ] ],
         "jack"   => [ [ "2023-11-25" ], [ "2023-11-27" ], [ "2023-11-30", "2023-12-03" ] ]
      }

      titles = [   # the following tasks are randomly assigned to the dates above for each user
         "get shit done",
         "stop doing shit",
         "take a shit",
         "get more shit done",
         "leave a shit",
         "get outrageous shit done",
         "start doing shit",
         "wipe your butt",
         "wash your underwear"
      ]

      tasks = { }

      titles.each { | title |   # populate tasks hash
         task_id = SecureRandom.uuid()
         tasks[ task_id ] = {
            title:    title,
            type:     :text,
            status:   STATUS.sample,
            contents: "task_id %s: contents of %s" % [ task_id, title ],
            links:    [ ]
         }
      }

      users.each { | uname, grid |
         redis.set( "user:#{ uname }:pw",   uname        )   # pw is username
         redis.set( "user:#{ uname }:grid", grid.to_json )
         grid.each { | dateA |   # not checking for sequential dates

            case dateA.length
               when 1
                  key = "date:#{ uname }:#{ dateA[ 0 ] }"
                  redis.set( key, task_list( tasks, key ).to_json )

               when 2
                  startD = Date.strptime( dateA[ 0 ], DATE_P )
                  stopD  = Date.strptime( dateA[ 1 ], DATE_P )
                  startD.step( stopD ) { | dateD |
                     dateS = dateD.strftime( DATE_P )
                     key   = "date:#{ uname }:#{ dateS }"
                     redis.set( key, task_list( tasks, key ).to_json )
                  }

               else
                  raise( "invalid date sub-array" )
            end
         }
      }

      tasks.each { | task_id, task |          # do this after users loop which populates links in each task
         key = "task:" + task_id
         redis.set( key, task.to_json )
      }
   end

   #--------------------------------------------------------------------------------------------------------------------

   def task_list( tasksH, key )   # return random length list of unique tasks randomly sampled from tasksH keys
      out = [ ]
      task_idA = tasksH.keys

      ( rand( tasksH.length ) + 1 ).times { | _ |   # rand( N ) returns int [0,N)
         task_id = task_idA.sample
         out << task_id
         tasksH[ task_id ][ :links ] << key
         task_idA.delete( task_id )                 # so same task is not selected more than once
      }

      out
   end

end      # class Populate
