# TODO
# add http-client emitting request and printing request and response

#require "http-client"

IP   = "localhost"
PORT = 4567

$\ = "\n"

#-----------------------------------------------------------------------------------------------------------------------
########################################################################################################################

class Iter

   def initialize( type )
      @data = case type         # iterator input arrays cannot be empty and cannot contain nil
         when :task_id then %w[ 101 202 303 ]
         when :date    then %w[ 11-20-2023 11-21-2023 12-2-2023 1-5-2024 ]
         when :start   then %w[ 11-20-2023 11-21-2023 12-2-2023 1-5-2024 ]
         when :stop    then %w[ 11-20-2023 11-21-2023 12-2-2023 1-5-2024 ]   # won't use stop unless also using start
         else raise( "invalid iterator type: " + type )
      end

      @it = @data.each
   end

   #---------------------------------------------------------------------------------------------------------------------

   def next( )
      begin
         @it.next
      rescue
         @it.rewind
         nil
      end
   end

end

########################################################################################################################

def assemble( prev_it, pre, s )
   out = ""
   out << prev_it + "/" if ! prev_it.empty?
   out << pre     + "/" if ! pre.empty?
   out << s
end

#-----------------------------------------------------------------------------------------------------------------------

def send( method, url, stack )
   if stack != nil
      ptr = 0

      # [ [ "", "start", Iter.new( :start ) ], [ "", "stop", Iter.new( :stop ) ] ]

      while ptr >= 0                 # stack entry: [ prev_it, keyS, iter_instance ]
         prev_it, key, iter = stack[ ptr ]

         if ptr < stack.length - 1   # not last iterator
            s = iter.next
            if s != nil
               prev_it2 = ( ptr == 0 ) ? "?" : prev_it + "&"   # prev_it should be empty if ptr==0
               ptr += 1
               stack[ ptr ][ 0 ] = prev_it2 + key + "=" + s
            else                     # iterator rewinds when end is reached
               ptr -= 1
            end

         else                        # last iterator
            while ( s = iter.next ) != nil
               prev_it2 = ( ptr == 0 ) ? "?" : prev_it + "&"   # prev_it should be empty if ptr==0
               print( "#{ method } #{ url }#{ prev_it2 + key + "=" + s }" )
            end                      # iterator rewinds when end is reached
            ptr -= 1
         end   # if ptr

      end     # while

   else
      print( "#{ method } #{ url }" )
   end

   # send request to client
end

########################################################################################################################

endpoints = [
   {
      method: :post,
      url:    "/task",   # string or url component iterator stack
      body:   [ ]
   },
   {
      method: :get,
      url:    [ [ "", "/task", Iter.new( :task_id ) ] ]
   },
   {
      method: :put,
      url:    [ [ "", "/task", Iter.new( :task_id ) ] ],
      body:   [ ]   # task object
   },
   {
      method: :put,
      url:    [ [ "", "/task/move", Iter.new( :task_id ) ] ],
      body:   [ ]   # { current: date, new: date, task_list: task_id array} ; could make entire req body the output of an Iter instance
   },
   {
      method: :delete,
      url:    [ [ "", "/task", Iter.new( :task_id ) ] ],
      params: [ [ "", "date", Iter.new( :date ) ] ]
   },
   {
      method: :post,
      url:    [ [ "", "/date", Iter.new( :date ) ] ],
      body:   [ ]   # task_id array
   },
   {
      method: :get,
      url:    "/date",
      params: [ [ "", "start", Iter.new( :date ) ] ]
   },
   {
      method: :get,
      url:    "/date",
      params: [ [ "", "start", Iter.new( :start ) ], [ "", "stop", Iter.new( :stop ) ] ]
   },
   {
      method: :put,
      url:    [ [ "", "/date", Iter.new( :date ) ] ],
      body:   [ ]   # task_id array
   },
   {
      method: :delete,
      url:    [ [ "", "/date", Iter.new( :date ) ] ]
   }
]

#-----------------------------------------------------------------------------------------------------------------------

endpoints.each { | endH |   # stack entry: [ prev_iter_outputS, preS, iter_instance ]
   method = endH[ :method ]
   params = endH[ :params ]
   body   = endH[ :body   ]

   if endH[ :url ].class == String
      send( method, endH[ :url ], params )

   else               # assume class Array; all endpoints have only a single url stack
      stack = endH[ :url ]
      ptr   = 0

      while ptr >= 0
         prev_it, pre, iter = stack[ ptr ]

         if ptr < stack.length - 1   # not last iterator
            s = iter.next
            if s != nil
               ptr += 1
               stack[ ptr ][ 0 ] = assemble( prev_it, pre, s )   # write this stage's output into next stage's input
            else                     # iterator rewinds when end is reached
               ptr -= 1
            end

         else                        # last iterator
            while ( s = iter.next ) != nil
               send( method, assemble( prev_it, pre, s ), params )
            end                      # iterator rewinds when end is reached
            ptr -= 1
         end   # if ptr <

      end      # while

   end         # if endH[ :url ].class
}
