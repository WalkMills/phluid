# TODO
# add http-client emitting request and printing request and response

#require "http-client"

IP   = "localhost"
PORT = 4567

$\ = "\n"

#-----------------------------------------------------------------------------------------------------------------------
########################################################################################################################

class Iter

   def initialize( type )
      @type = type
      @data = case type         # iterator input arrays cannot be empty and cannot contain nil
         when :task_id then %w[ 101 202 303 ]
         when :date    then %w[ 11-20-2023 11-21-2023 12-2-2023 1-5-2024 ]
         when :start   then %w[ 11-20-2023 11-21-2023 12-2-2023 1-5-2024 ]
         when :stop    then %w[ 11-20-2023 11-21-2023 12-2-2023 1-5-2024 ]   # won't use stop unless also using start
         when :x       then %w[ a b c ]
         else raise( "invalid iterator type: " + type )
      end

      @it = @data.each
   end

   #---------------------------------------------------------------------------------------------------------------------

   def next( )
      begin
         @it.next
      rescue
         @it.rewind
         nil
      end
   end

   #---------------------------------------------------------------------------------------------------------------------

   def to_s( )
      "Iter( #{ @type } )"
   end

   def inspect( )
      to_s()
   end

end

########################################################################################################################

class Acc
   attr_accessor( :url, :params, :body )

   def initialize( )
      @url    = ""
      @params = { }
      @body   = ""
   end
end

########################################################################################################################

def make_stack( compA, assyP )   # argument is array of components; return is [ stack, post ]
   stack  = [ ]
   post   = ""                   # concatenation of all strings/constants in compA after last iterator
   nxt_el = [ "", "", nil ]      # the next element to be pushed onto the stack; [ previous, inter, iter ]
   pos    = :first               # :first, :mid, :last position of component in compA
   compC  = nil                  # so compC is scoped below loop

   compA.each_with_index { | comp, index |
      compC = comp.class

      if compC == String                                      # accumulate constant components in [0]
         nxt_el[ 1 ] = assyP.call( nxt_el[ 1 ], comp, pos )   # assyP should append comp to previously accumulated value

      elsif compC == Iter
         nxt_el[ 2 ] = comp
         stack << nxt_el
         nxt_el = [ "", "", nil ]

      else
         raise( "invalid component class: " + compC.to_s )
      end

      pos = ( index == compA.length - 2 ) ? :last : :mid   # set pos for next iteration
   }

   post = nxt_el[ 1 ] if compC == String   # if last element of compA was a string/constant
   [ stack, post ]                         # if all comps were strings/constant then stack will be empty
end

########################################################################################################################

def make_stack_old( compA )       # this is for URL's; argument is array of components; return is [ stack, post ]
   stack  = [ ]
   post   = ""                # concatenation of all strings in compA after last iterator
   nxt_el = [ "", "", nil ]   # the next element to be pushed onto the stack; [ prev_iter_outS, preS, iter ]
   prevC  = nil               # class of previous component (so successve strings can be concatenated)

   compA.each { | comp |
      compC = comp.class

      if compC == String
         if prevC == String || prevC.nil?
            nxt_el[ 1 ] << "/" << comp    # TODO: all components (constant string and variable iterator) are elements in an array
         else                             # Iter (any other class caused exception on previous loop iteration)
            nxt_el[ 1 ] = comp
         end

      elsif compC == Iter
         nxt_el[ 1 ] << "/"         # so "/" appears between previous component and output of this iterator
         nxt_el[ 2 ] = comp.to_s    # TODO: remove to_s!!!!!!!!!
         stack << nxt_el
         nxt_el = [ "", "", nil ]

      else
         raise( "invalid component class: " + compC )
      end

      prevC = compC
   }

   post  = nxt_el[ 1 ] if prevC == String   # if last element of compA was a string
   stack = nxt_el[ 1 ] if stack.empty?      # return a string (not array) if no iterators in compA
   [ stack, post ]
end

#-----------------------------------------------------------------------------------------------------------------------

def send( method, url, stack )
   if stack != nil
      ptr = 0

      # [ [ "", "start", Iter.new( :start ) ], [ "", "stop", Iter.new( :stop ) ] ]

      while ptr >= 0                 # stack entry: [ prev_it, keyS, iter_instance ]
         prev_it, key, iter = stack[ ptr ]

         if ptr < stack.length - 1   # not last iterator
            s = iter.next
            if s != nil
               prev_it2 = ( ptr == 0 ) ? "?" : prev_it + "&"   # prev_it should be empty if ptr==0
               ptr += 1
               stack[ ptr ][ 0 ] = prev_it2 + key + "=" + s
            else                     # iterator rewinds when end is reached
               ptr -= 1
            end

         else                        # last iterator
            while ( s = iter.next ) != nil
               prev_it2 = ( ptr == 0 ) ? "?" : prev_it + "&"   # prev_it should be empty if ptr==0
               print( "#{ method } #{ url }#{ prev_it2 + key + "=" + s }" )
            end                      # iterator rewinds when end is reached
            ptr -= 1
         end   # if ptr

      end      # while

   else
      print( "#{ method } #{ url }" )
   end

   # send request to client
end

########################################################################################################################

endpoints = [
   {
      method: :post,
      url:    [ "task" ],   # string or url component iterator stack
      body:   [ ]
   },
   {
      method: :get,
      url:    [ "task", Iter.new( :task_id ) ]
   },
   {
      method: :put,
      url:    [ "task", Iter.new( :task_id ) ],
      body:   [ ]   # task object
   },
   {
      method: :put,
      url:    [ "task", "move", Iter.new( :task_id ) ],
      body:   [ ]   # { current: date, new: date, task_list: task_id array} ; could make entire req body the output of an Iter instance
   },
   {
      method: :delete,
      url:    [ "task", Iter.new( :task_id ) ],
      params: [ [ "", "date", Iter.new( :date ) ] ]
   },
   {
      method: :post,
      url:    [ "date", Iter.new( :date ) ],
      body:   [ ]   # task_id array
   },
   {
      method: :get,
      url:    [ "date" ],
      params: [ [ "", "start", Iter.new( :date ) ] ]
   },
   {
      method: :get,
      url:    [ "date" ],
      params: [ [ "", "start", Iter.new( :start ) ], [ "", "stop", Iter.new( :stop ) ] ]
   },
   {
      method: :put,
      url:    [ "date", Iter.new( :date ) ],
      body:   [ ]   # task_id array
   },
   {
      method: :delete,
      url:    [ "date", Iter.new( :date ) ]
   },
   {
      method: :delete,
      url:    [ "date", Iter.new( :date ), Iter.new( :x ), "task", Iter.new( :task_id ), "final" ]
   }
]

#-----------------------------------------------------------------------------------------------------------------------

urlP = ->( prev, comp, pos ) {
   raise( "pos :first with non-empty prev: " + prev ) if pos == :first && ! prev.empty?
   prev + "/" + comp   # if :first prev should be empty
}

paramP = ->( prev, comp, pos ) {
   if pos == :first   # if :first prev should be empty
      "?" + comp      # actually need key=value
   else
      prev + "&" + comp
   end
}

#-----------------------------------------------------------------------------------------------------------------------

endpoints.each { | endH |
   method, url, params, body = endH.fetch_values( :method, :url, :params, :body ) { | key | nil }   # return nil if key not found
   stack, post = make_stack( url, urlP )
   print( "\nstack: ", stack )
   print( "post: ", post )

   if stack.empty?
      print( "url: ", post  )

   else
      ptr = 0

      if stack[ 0 ][ 0 ].empty?                # first iterator is not preceded by strings/constants
         pos = :first
      elsif stack.length == 1 && post.empty?   # only one iterator with no strings/constants after
         pos = :last
      else                                     # TODO: how to handle case of only one component? i.e. both first and last
         pos = :mid
      end

      pos = nil

      while ptr >= 0
         prev, inter, iter = stack[ ptr ]
         prev += inter               # urlP was called on inter in make_stack()

         if ptr < stack.length - 1   # not last iterator
            s = iter.next

            if s != nil
               ptr += 1
               stack[ ptr ][ 0 ] = urlP.call( prev, s, pos )   # write this stage's output into next stage's input
            else                     # iterator rewinds when end is reached
               ptr -= 1
            end

         else                        # last iterator

            while ( s = iter.next ) != nil
               final  = urlP.call( prev, s, :last )
               final += post if ! post.empty?   # urlP was called on post in make_stack()
               print( "url: ", final )
            end                      # iterator rewinds when end is reached

            ptr -= 1
         end   # if ptr < stack.length - 1

      end      # while

   end         # if stack.empty?

}

exit

__END__
#-----------------------------------------------------------------------------------------------------------------------

endpoints.each { | endH |
   method, url, params, body = endH.fetch_values( :method, :url, :params, :body ) { | key | nil }   # return nil if key not found
   url_stack, url_post = make_stack( url, urlP )

   if ! url_stack.empty?
      stack = [ [ url_stack, urlP, url_post ] ]
   else
      stack = [ [ url_post ], urlP, "" ]
   end

   if ! params.empty?      # TODO: does not cover case where params only has constants
      params_stack, params_post = make_stack( params, paramsP )
      stack << [ params_stack, paramsP, params_post ]
   end

   traverse_stack( stack, Container )

   if ! url_post.empty?
      # if param_stack==Array, prepend url_post to param_stack[0][1]
      # else prepend to param_stack
   end

   print( "\n" )
   print( "stack: ", stack )
   print( "post: ",  post  )
}

#-----------------------------------------------------------------------------------------------------------------------

endpoints.each { | endH |   # stack entry: [ prev_iter_outputS, preS, iter_instance ]

   if url.class == String
      send( method, url, params )

   else               # assume class Array; all endpoints have only a single url stack
      stack = url
      ptr   = 0
      url   = ""

      while ptr >= 0
         comp = stack[ ptr ]
         if comp.class == String
            url << "/" << comp
            next
         elsif comp.class == Iter

         else
            raise( "invalid url component" )
         end

         prev_it, pre, iter = stack[ ptr ]

         if ptr < stack.length - 1   # not last iterator
            s = iter.next
            if s != nil
               ptr += 1
               stack[ ptr ][ 0 ] = assemble( prev_it, pre, s )   # write this stage's output into next stage's input
            else                     # iterator rewinds when end is reached
               ptr -= 1
            end

         else                        # last iterator
            while ( s = iter.next ) != nil
               send( method, assemble( prev_it, pre, s ), params )
            end                      # iterator rewinds when end is reached
            ptr -= 1
         end   # if ptr <

      end      # while

   end         # if url.class
}
