#!ruby
########################################################################################################################
#-----------------------------------------------------------------------------------------------------------------------
    #-------------------------------------------------------------------------------------------------------------------

require "redis"
require "json"
require "date"

$\ = "\n"

########################################################################################################################
# test behavior or json parsing class

class NilClass
    def to_s( )
        "nil"
    end
end

########################################################################################################################

[ "", "[]", "{}", "xxx", "123", "||+" ].each { | input |
   printf( "%s : ", input )

   begin
      output = JSON.parse( input )
      printf( "%s (%s)\n", output, output.class )
   rescue JSON::ParserError => e              # empty string; string only; nil; junk string
      printf( "%s\n", e.message )
   end
}

exit

########################################################################################################################

def add_date( dateS, gridA )   # insert dateS into grid
   return [ dateS ] if gridA.empty?
   dateD     = Date.strptime( dateS, "%m-%d-%Y" )
   earliestD = Date.strptime( gridA[ 0 ][ 0 ], "%m-%d-%Y" )

   if dateD < earliestD            # insert before first element
      if dateD.next == earliestD   # dateS is immediately before first date
         if gridA[ 0 ].length == 1
            gridA[ 0 ] = [ dateS, gridA[ 0 ][ 0 ] ]
         else
            gridA[ 0 ][ 0 ] = dateS
         end

      else                          # dateS not immediately before first date
         gridA.unshift( [ dateS ] )
      end

   else

      gridA.reverse_each.with_index { | rangeA, i |
         prev_dateD = Date.strptime( rangeA[ -1 ], "%m-%d-%Y" )

         if dateD > prev_dateD
            insert_after( gridA, dateS, dateD, prev_dateD, gridA.length - 1 - i, i == 0 )   # insert after gridA[i]
            break   # insert_before modifies gridA but loop does not continue
         end
      }

   end

   gridA
end

#-----------------------------------------------------------------------------------------------------------------------

def insert_after( gridA, dateS, dateD, prev_dateD, i, last )   # prev_dateD = latest before dateD; i = prev_dateD index

   if dateD == prev_dateD.next       # dateD immediately after gridA[ i ]

      if ! last && dateD.next == Date.strptime( gridA[ i + 1 ][ 0 ], "%m-%d-%Y" )   # dateD fills gap btw i & i+1
         gridA[ i ] = [ gridA[ i ][ 0 ], gridA[ i + 1 ][ -1 ] ]   # merge gridA[ i ] with gridA[ i+1 ]
         gridA.delete_at( i + 1 )                                 # and remove one of them
      elsif gridA[ i ].length == 1   # convert single date gridA[ i ] into range
         gridA[ i ] << dateS
      else
         gridA[ i ][ -1 ] = dateS    # expand gridA[ i ] range 1 day later
      end

   elsif ! last && dateD.next == Date.strptime( gridA[ i + 1 ][ 0 ], "%m-%d-%Y" )   # dateD immediately b4 gridA[i+1]

      if gridA[ i + 1 ].length == 1
         gridA[ i + 1 ].unshift( dateS )   # convert single date gridA[ i+1 ] into range
      else
         gridA[ i + 1 ][ 0 ] = dateS       # expand gridA[ i+1 ] range 1 day earlier
      end

   else
      gridA.insert( i - gridA.length, [ dateS ] )   # dateD not adjacent to any; insert single element btw i & i+1
   end
end

#-----------------------------------------------------------------------------------------------------------------------

print( "\n\n", "*" * 10, "\n" )

{
   "empty grid (11-15-23)" => [
      "11-15-23",
      [ ] ],

   "single element grid, after last, adjacent to single (11-15-23)" => [
      "11-15-23",
      [ [ "11-14-23" ] ] ],

   "single element grid, after last, adjacent to double (11-15-23)" => [
      "11-15-23",
      [ [ "11-12-23", "11-14-23" ] ] ],

   "single element grid, before first, adjacent to single (11-15-23)" => [
      "11-15-23",
      [ [ "11-16-23" ] ] ],

   "single element grid, before first, adjacent to double (11-15-23)" => [
      "11-15-23",
      [ [ "11-16-23", "11-18-23" ] ] ],

   "single element grid, after last, non-adjacent to single (11-15-23)" => [
      "11-15-23",
      [ [ "11-12-23" ] ] ],

   "single element grid, after last, non-adjacent to double (11-15-23)" => [
      "11-15-23",
      [ [ "11-10-23", "11-12-23" ] ] ],

   "single element grid, before first, non-adjacent to single (11-15-23)" => [
      "11-15-23",
      [ [ "11-17-23" ] ] ],

   "single element grid, before first, non-adjacent to double (11-15-23)" => [
      "11-15-23",
      [ [ "11-17-23", "11-18-23" ] ] ],

   "after last, adjacent to single (11-15-23)" => [
      "11-15-23",
      [ [ "11-8-23" ], [ "11-10-23", "11-12-23" ], [ "11-14-23" ] ] ],

   "after last, adjacent to double (11-15-23)" => [
      "11-15-23",
      [ [ "11-8-23" ], [ "11-10-23" ], [ "11-12-23", "11-14-23" ] ] ],

   "after last, non-adjacent to single (11-16-23)" => [
      "11-16-23",
      [ [ "11-8-23" ], [ "11-10-23", "11-12-23" ], [ "11-14-23" ] ] ],

   "after last, non-adjacent to double (11-16-23)" => [
      "11-16-23",
      [ [ "11-8-23" ], [ "11-10-23" ], [ "11-12-23", "11-14-23" ] ] ],

   "before first, adjacent to single (11-7-23)" => [
      "11-7-23",
      [ [ "11-8-23" ], [ "11-10-23", "11-12-23" ], [ "11-14-23" ] ] ],

   "before first, adjacent to double (11-7-23)" => [
      "11-7-23",
      [ [ "11-8-23", "11-10-23" ], [ "11-12-23" ], [ "11-14-23" ] ] ],

   "before first, non-adjacent to single (11-6-23)" => [
      "11-6-23",
      [ [ "11-8-23" ], [ "11-10-23", "11-12-23" ], [ "11-14-23" ] ] ],

   "before first, non-adjacent to double (11-6-23)" => [
      "11-6-23",
      [ [ "11-8-23", "11-10-23" ], [ "11-12-23" ], [ "11-14-23" ] ] ],

   "between single-single, non-adjacent to both (11-12-23)" => [
      "11-12-23",
      [ [ "11-6-23", "11-8-23" ], [ "11-10-23" ], [ "11-14-23" ] ] ],

   "between single-double, non-adjacent to both (11-14-23)" => [
      "11-14-23",
      [ [ "11-8-23" ], [ "11-12-23" ], [ "11-16-23", "11-18-23" ] ] ],

   "between double-single, non-adjacent to both (11-14-23)" => [
      "11-14-23",
      [ [ "11-6-23" ], [ "11-10-23", "11-12-23" ], [ "11-16-23" ] ] ],

   "between double-double, non-adjacent to both (11-14-23)" => [
      "11-14-23",
      [ [ "11-6-23" ], [ "11-10-23", "11-12-23" ], [ "11-16-23", "11-18-23" ] ] ],

   "between single-single, adjacent to earlier (11-7-23)" => [
      "11-7-23",
      [ [ "11-6-23" ], [ "11-9-23" ], [ "11-16-23" ] ] ],

   "between single-double, adjacent to earlier (11-7-23)" => [
      "11-7-23",
      [ [ "11-6-23" ], [ "11-9-23", "11-12-23" ], [ "11-16-23" ] ] ],

   "between double-single, adjacent to earlier (11-7-23)" => [
      "11-7-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23" ], [ "11-16-23" ] ] ],

   "between double-double, adjacent to earlier (11-7-23)" => [
      "11-7-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23", "11-12-23" ], [ "11-16-23" ] ] ],

   "between single-single, adjacent to later (11-8-23)" => [
      "11-8-23",
      [ [ "11-6-23" ], [ "11-9-23" ], [ "11-16-23" ] ] ],

   "between single-double, adjacent to later (11-8-23)" => [
      "11-8-23",
      [ [ "11-6-23" ], [ "11-9-23", "11-12-23" ], [ "11-16-23" ] ] ],

   "between double-single, adjacent to later (11-8-23)" => [
      "11-8-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23" ], [ "11-16-23" ] ] ],

   "between double-double, adjacent to later (11-8-23)" => [
      "11-8-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23", "11-12-23" ], [ "11-16-23" ] ] ],

   "between single-single, adjacent to both (11-15-23)" => [
      "11-15-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23", "11-12-23" ], [ "11-14-23" ], [ "11-16-23" ] ] ],

   "between single-double, adjacent to both (11-15-23)" => [
      "11-15-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23", "11-12-23" ], [ "11-14-23" ], [ "11-16-23", "11-18-23" ] ] ],

   "between double-single, adjacent to both (11-15-23)" => [
      "11-15-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23" ], [ "11-12-23", "11-14-23" ], [ "11-16-23" ] ] ],

   "between double-double, adjacent to both (11-15-23)" => [
      "11-15-23",
      [ [ "11-2-23", "11-6-23" ], [ "11-9-23", "11-10-23" ], [ "11-12-23", "11-14-23" ], [ "11-16-23", "11-18-23" ] ] ]

}.each { | label, ( dateS, gridA ) |
   gridA = add_date( dateS, gridA )
   print( "\n", "-" * 50 )
   print( label )
   print( gridA )
}

exit

########################################################################################################################
# json parsing

[
    "this is a string",
    '"this is a string 2"',
    "1234",
    '"1234"',
    '"nov 6 2023"',
    '"nov 6, 2023"',
    '"nov 6 23"',
    '[ "string", 1234, "110623", "nov 6 2023", "nov 6, 2023", "nov 6 23" ]'
].each { | i |
    print( "------------" )
    print( i )

    begin
        o = JSON.parse( i )
    rescue Exception => e
        print( "json ex: ", e.message )
        next
    end

    print( "%s (%s)" % [ o, o.class ] )
}

[
    "nov 6 2023",
    "nov 6, 2023",
    "nov 6 23"
].each { | i |
    print( "------------" )
    print( i )
    m, d, y = i.split( /[ ,]+/ )
    print( "y:%s : m:%s : d:%s" % [ y, m, d ] )
    t = Time.new( y, m, d )
    print( "%s (%s)" % [ t, t.class ] )
}

exit

########################################################################################################################
# test behavior of redis

pid = spawn( "redis-server" )
print( "*** starting redis in process ", pid )
sleep( 2 )                            # pause this process to give server process time to launch
options = {                           # prior to dell laptop, Redis.new failed with default connection options
   reconnect_attempts: [ 0.5, 0.5 ]   # after first attempt, 2 reconnect attempts with 0.5 second delay before each
}                                     # starting with dell laptop, revert to unadorned Redis.new
redis = Redis.new( )   # default is localhost:6379 with no password; automatically connects to existing db

redis.flushall                # clear any existing database
# empty
out = redis.get( "a" )        # get not existing
print( "get( nx ): ", out )
# empty
out = redis.sadd( "s1", %w[ x y ] )
print( "sadd( nx ): ", out )
# s1(["x","y"])
begin
    out = redis.get( "s1" )    # get not string
    print( "get( not string ): ", out )
rescue Redis::CommandError => e
    print( "get( not string ): exception.msg: ", e.message )
end
# s1(["x","y"])
out = redis.set( "a", "a1" )   # set not existing
print( "set( nx ): ", out )
# a(a1), s1(["x","y"])
out = redis.set( "a", "a2" )   # set existing
print( "set( ex ): ", out )
# a(a2), s1(["x","y"])
out = redis.set( "b", "b1", nx: true )  # only set if key does not exist; and key does not exist
print( "set( nx/nx ): ", out )
# a(a2), b(b1), s1(["x","y"])
out = redis.set( "b", "b2", nx: true )  # only set if key does not exist; and key exists
print( "set( nx/ex ): ", out )
# a(a2), b(b1), s1(["x","y"])
out = redis.set( "c", "c1", xx: true )   # only set if key already exists; and key does not exist
print( "set( xx/nx ): ", out )
# a(a2), b(b1), s1(["x","y"])
out = redis.set( "a", "a3", xx: true )   # only set if key already exists; and key exists
print( "set( xx/xx ): ", out )
# a(a3), b(b1), s1(["x","y"])
begin
    out = redis.renamenx( "c", "d" )   # renamenx: source does not exist; target does not exist
    print( "renamenx( nx, nx ): ", out )
rescue Redis::CommandError => e
    print( "renamenx( nx, nx ): exception.msg: ", e.message )
end
# a(a3), b(b1), s1(["x","y"])
begin
    out = redis.renamenx( "c", "a" )   # renamenx: source does not exist; target exists
    print( "renamenx( nx, ex ): ", out )
rescue Redis::CommandError => e
    print( "renamenx( nx, ex ): exception.msg: ", e.message )
end
# a(a3), b(b1), s1(["x","y"])
out = redis.renamenx( "b", "c" )   # renamenx: source exists; target does not exist
print( "renamenx( ex, nx ): ", out )
# a(a3), c(b1), s1(["x","y"])
out = redis.renamenx( "a", "c" )   # renamenx: source exists; target exists
print( "renamenx( ex, ex ): ", out )
# a(a3), c(b1), s1(["x","y"])
out = redis.smembers( "s" )   # get set not existing
print( "smembers( nx ): ", out )
print( "output class: ", out.class )
# a(a3), c(b1), s1(["x","y"])
out = redis.del( "d" )        # delete non-existing
print( "del( nx string ): ", out )
# a(a3), c(b1), s1(["x","y"])
out = redis.del( "c" )        # delete existing string
print( "del( xx string ): ", out )
# a(a3), s1(["x","y"])
out = redis.del( "s1" )        # delete existing set
print( "del( ex set ): ", out )
# a(a3)
redis.scan_each { | key |
    case redis.type( key )
        when "string"
            print( key, " : ", redis.get( key ) )
        when "set"
            print( key, " : ", redis.smembers( key ) )
        else
            print( "unknown key type" )
    end
}

# set set not existing
# set set existing

redis.quit

########################################################################################################################
