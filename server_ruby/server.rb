#!ruby

########################################################################################################################
#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--
#-----------------------------------------------------------------------------------------------------------------------
   #--------------------------------------------------------------------------------------------------------------------

# at_exit {
#    ps_out = %x[ ps -a ]
#    psM = ps_out.match( /\n +(\d+) +.+redis-server\n/m )

#    if ! psM.nil?
#       print( "ps out" )
#       print( ps_out )
#       print( "sending kill to #{ psM[ 1 ] }" )
#       status = system( "kill -2 #{ psM[ 1 ] }" )
#       print( "kill ", status ? "successful" : "unsuccessful" )
#    else
#       print( "\n*** no match on ps output" )
#       print( ps_out )
#    end

#    print( "\n*** process #{ Process.pid } terminating" )
# }

########################################################################################################################

require "sinatra/base"
require "redis"
require "haml"
require "json"
require "securerandom"
require "date"
require "pp"
require_relative "populate"

$\ = "\n"

########################################################################################################################

class Container                   # used to pass state to the haml template as instance vars
   def initialize( state )        # so external state can be easily identified in haml as starting with "@"
      state.each { | name, value |
         instance_variable_set( "@" + name.to_s, value )
      }
   end
end

########################################################################################################################

class Phluid < Sinatra::Base

HAML_FILE      = ENV[ "PHLUID_ROOT" ] + "/assets/jquery.haml"
CSS_FILE       = ENV[ "PHLUID_ROOT" ] + "/assets/phluid.css"
JQUERY_FILE    = ENV[ "PHLUID_ROOT" ] + "/client_jquery/jquery-3.7.0.min.js"
REACT_FILE     = ENV[ "PHLUID_ROOT" ] + "/client_react/react-18.2.0-development.js"
REACT_DOM_FILE = ENV[ "PHLUID_ROOT" ] + "/client_react/react-dom-18.2.0-development.js"
BABEL_FILE     = ENV[ "PHLUID_ROOT" ] + "/client_react/babel-standalone.min.js"

DATE_P = "%Y-%m-%d"                # convert between instances of Date and date key strings
# coarse grained check for valid date key format because...
DATE_R = /[12]\d{3}-\d{2}-\d{2}/   # ...Date.strptime() doesn't care about leading zeroes on month or day of month
DISPLAY_DATE_P = "%a %b %d %Y"     # convert instance of Date to date string displayed in date html elements

########################################################################################################################

configure {   # executed once at startup
   # defaults
   pop   = false
   fwork = :jquery
   client_file = ENV[ "PHLUID_ROOT" ] + "/client_jquery/app.js"
   # process any command line arguments
   ARGV.each { | arg |
      case arg
         when "-pop"    then pop = true
         when "-jquery" then next         # defaults already set
         when "-react"
            fwork = :react
            client_file = ENV[ "PHLUID_ROOT" ] + "/client_react/app.js"
         else
            raise( "invalid command line option: " + arg )
      end
   }

   pid = spawn( "redis-server" )
   # had sleep(2) here in the past
   options = { reconnect_attempts: [ 0.5, 0.5 ] }   # after first attempt, 2 reconnect attempts with 0.5 second delay btw each
   redis   = Redis.new( options )                   # connects to existing db or creates new; default is localhost:6379 with no pw
   Populate.new( redis ) if pop

   #set( :server, "puma" )
   enable( :sessions )
   #set( :mustermann_opts, { type: :regexp } )   # use same regex engine as ruby (this doesn't appear to work)
   set( :redis,     redis )
   set( :fwork,     fwork )
   set( :css,       CSS_FILE       )
   set( :jquery,    JQUERY_FILE    )
   set( :react,     REACT_FILE     )
   set( :react_dom, REACT_DOM_FILE )
   set( :babel,     BABEL_FILE     )
   set( :client,    client_file    )
}

########################################################################################################################

helpers {   # methods available to all routes

#-----------------------------------------------------------------------------------------------------------------------

def read( key, mode = nil )      # returns json-parsed data unless mode = :raw; returns nil if key does not exist
   value = settings.redis.get( key )

   if mode == :raw
      value
   else
      return nil if value.nil?   # key does not exist
      JSON.parse( value )
   end

rescue Redis::CommandError       # value is not a string
   raise( "read: non-string value" )

rescue JSON::JSONError           # invalid json
   raise( "read: invalid json" )
end

#-----------------------------------------------------------------------------------------------------------------------

def write( key, value, mod = nil )   # converts value to json and writes to key; returns true or nil
   value = value.to_json

   case mod
      when :nx                                              # key must not already exist
         out = settings.redis.set( key, value, nx: true )   # out should only be true or false
         ( out == false ) ? nil : out

      when :xx                                              # key must already exist
         out = settings.redis.set( key, value, xx: true )
         ( out == false ) ? nil : out

      else                                                  # existence of key not checked
         out = settings.redis.set( key, value )
         ( out == "OK" ) ? true : nil                       # out should only be "OK" whether or not key exists
   end
end

#-----------------------------------------------------------------------------------------------------------------------

def make_grid( uname, datesA )            # grid never directly written with data from client
   out = [ ]

   datesA.each { | rangeA |
      case rangeA.length
         when 1
            out << [ rangeA[ 0 ], make_task_list( uname, rangeA[ 0 ] ) ]

         when 2
            startD = Date.strptime( rangeA[ 0 ], DATE_P )
            stopD  = Date.strptime( rangeA[ 1 ], DATE_P )

            startD.step( stopD ) { | dateD |
               dateS = dateD.strftime( DATE_P )
               out << [ dateS, make_task_list( uname, dateS ) ]
            }

         else   # 0 or >2 elements in date range array
            raise( "%d elements in date sub-array" % dateA.length )
      end       # case
   }

   out
end

#-----------------------------------------------------------------------------------------------------------------------

def make_task_list( uname, dateS )
   key = "date:%s:%s" % [ uname, dateS ]
   task_list = read( key )
   raise( "date key does not exist: " + key ) if task_list.nil?

   task_list.map { | task_id |
      key  = "task:" + task_id
      task = read( key )
      raise( "task key does not exist: " + key ) if task.nil?
      task.delete( "links" )   # links not currently sent to browser
      task[ :id ] = task_id
      task
   }
end

#-----------------------------------------------------------------------------------------------------------------------
# if convert==:json: parses data as json string and returns the result
# else: returns unmodified data except for :date type which returns instance of Date

def data_check( data, type, convert = nil )   # for checking writes; all reads must have been preceded by a past write

   if convert == :json
      begin
         data = JSON.parse( data )
      rescue
         raise( "invalid #{ type } json" )
      end
   end

   case type
      when :task_id      # cannot start with underscore and cannot contain colons
         raise( "invalid task id: " + data ) if data.class != String || data[ 0 ] == "_" || data[ ":" ] != nil

      when :date   # returns Date instance
         raise( "invalid date: " + data ) if data.class != String || data.match( DATE_R ).nil?

         begin
            dataD = Date.strptime( data, DATE_P )   # is string valid calendar date
         rescue
            raise( "invalid date: " + data )
         end

         data = dataD   # 'data' is returned at end of this function

      when :task         # link field not required (and ignored) in request bodies
         raise( "invalid task" ) if data.class != Hash

         %w[ title type status contents ].each { | prop |
            raise( "invalid task: missing #{ prop } property" ) if ! data.has_key?( prop )
         }

      when :task_list
         raise( "invalid task list: not Array" ) if data.class != Array

         data.each { | task_id |
            data_check( task_id, :task_id )
         }

      when :post_task
         raise( "post task body must be Hash" ) if data.class != Hash

         [ [ "date",      :date      ],   # 'date' property not changed from string to Date instance
           [ "task_list", :task_list ],
           [ "new",       :task      ],
           [ "position"              ] ].each { | prop, type |
            raise( "invalid request: missing #{ prop } property" ) if ! data.has_key?( prop )
            data_check( data[ prop ], type ) if ! type.nil?
         }

      when :move_task
         raise( "move task body must be Hash" ) if data.class != Hash

         [ [ "current",   :date      ],   # 'current' and 'new' properties not changed from string to Date instance
           [ "new",       :date      ],
           [ "task_list", :task_list ] ].each { | prop, type |
            raise( "invalid request: missing #{ prop } property" ) if ! data.has_key?( prop )
            data_check( data[ prop ], type ) if ! type.nil?
         }

      when :grid
         raise( "invalid grid" ) if data.class != Array
         return data if data.empty?
         prev = Date.strptime( data[ 0 ][ 0 ], DATE_P ).prev_day   # initialize to day before first date

         data.each_with_index { | rangeA, i |
            case rangeA.length
               when 1
                  first = Date.strptime( rangeA[ 0 ], DATE_P )
                  raise( "grid[%d][-1] (%s) >= grid[%d][0] (%s)" % [ i - 1, prev, i, first ] ) if prev >= first
                  prev = first

               when 2
                  first  = Date.strptime( rangeA[ 0 ], DATE_P )
                  second = Date.strptime( rangeA[ 1 ], DATE_P )
                  raise( "grid[%d][-1] (%s) >= grid[%d][0] (%s)" % [ i - 1, prev, i, first ] ) if prev  >= first
                  raise( "grid[%d][0] (%s) >= grid[%d][1] (%s)"  % [ i, first, i, second   ] ) if first >= second
                  prev = second

               else
                  raise( "invalid grid element: " + rangeA )
            end
         }

      else
         raise( "invalid data check type: " + type )
   end

   data
end

#-----------------------------------------------------------------------------------------------------------------------

def insert_before( gridA, dateS, dateD, earliestD )   # insert dateS before gridA[ 0 ]; dateD is Date insance of dateS
   if dateD.next_day == earliestD                     # dateS is immediately before first date; modify first element

      if gridA[ 0 ].length == 1
         gridA[ 0 ] = [ dateS, gridA[ 0 ][ 0 ] ]

      elsif gridA[ 0 ].length == 2
         gridA[ 0 ][ 0 ] = dateS

      else
         raise( "invalid grid element: " + gridA[ 0 ] )
      end

   else                                               # dateS not immediately before first date; add new first element
      gridA.unshift( [ dateS ] )
   end

   gridA
end

#-----------------------------------------------------------------------------------------------------------------------
# insert dateS after gridA[ i ]
def insert_after( gridA, dateS, dateD, prev_dateD, i, last )      # prev_dateD=latest before dateD; i=prev_dateD index

   if dateD == prev_dateD.next_day                                # dateD adjacent after gridA[ i ]

      if ! last && dateD.next_day == Date.strptime( gridA[ i + 1 ][ 0 ], DATE_P )   # dateD fills gap btw i & i+1
         gridA[ i ] = [ gridA[ i ][ 0 ], gridA[ i + 1 ][ -1 ] ]   # merge gridA[ i ] with gridA[ i + 1 ]
         gridA.delete_at( i + 1 )                                 # and remove one of them

      elsif gridA[ i ].length == 1                                # convert single date gridA[ i ] into range
         gridA[ i ] << dateS

      elsif gridA[ i ].length == 2
         gridA[ i ][ -1 ] = dateS                                 # expand gridA[ i ] range 1 day later

      else
         raise( "invalid grid element: " + gridA[ i ] )
      end

   elsif ! last && dateD.next_day == Date.strptime( gridA[ i + 1 ][ 0 ], DATE_P )   # dateD not adjacent after gridA[i]
                                                                                    # and adjacent before gridA[i+1]
      if gridA[ i + 1 ].length == 1
         gridA[ i + 1 ].unshift( dateS )            # convert single date gridA[ i + 1 ] into range

      else
         gridA[ i + 1 ][ 0 ] = dateS                # move gridA[ i + 1 ] range 1 day earlier
      end

   else
      gridA.insert( i - gridA.length, [ dateS ] )   # dateD not adjacent to any; insert single element btw i & i+1
   end

   gridA
end

}   # helpers

########################################################################################################################

get( "/heartbeat" ) {
   headers( "Content-Type" => "application/json" )
   [ 200, "i am alive" ]
}

#-----------------------------------------------------------------------------------------------------------------------

get( "/read/:key" ) {                               # this route is for test purposes
   key   = params[ :key ]
   value = read( key, :raw )   # does not convert read data from json
   value = "key does not exist" if value.nil?
   body  = { key => value }
   headers( "Content-Type" => "application/json" )
   [ 200, body.to_json ]
}

#-----------------------------------------------------------------------------------------------------------------------

put( "/write/:key" ) {                              # this route is for test purposes
   headers( "Content-Type" => "application/json" )

   begin
      key  = params[ :key ]
      body = JSON.parse( request.body.read )        # body is: { value: string }
      raise( "request body missing 'value' property" ) if ! body.has_key?( "value" )
      out  = write( key, body[ "value" ] )
      [ 200, out.to_json ]

   rescue Exception => e
      [ 400, e.message ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

get( "/dump" ) {                                    # this route is for test purposes

   body = settings.redis.keys.sort.map { | key |
      key + ": " + read( key, :raw )                # body is array of strings of format "key: value" sorted by key
   }

   headers( "Content-Type" => "application/json" )
   [ 200, body.to_json ]
}

#-----------------------------------------------------------------------------------------------------------------------

get( "/login" ) {   # params: uname=Uname (required); pw=string (required)
   begin
      raise( "invalid url" ) if ! ( params.has_key?( :uname ) && params.has_key?( :pw ) )
      prefix  = "user:" + params[ :uname ] + ":"
      pw_hash = read( prefix + "pw", :raw )   # get string w/o json conversion
      raise( "pw key does not exist: %spw" % prefix ) if pw_hash.nil?
      raise( "pw does not match"                    ) if pw_hash != params[ :pw ]   # TODO: hash pw before comparing

      # TODO: this cookie should only exist for this user's current session, not persist to another session
      session[ :user ] = params[ :uname ]
      # TODO: each sinatra instance generates a random secret to encode session state
      # TODO: however, all sinatra instances in the same cluster should use the same secret
      datesA = read( prefix + "grid" )

      if ! datesA.nil?
         gridA = make_grid( params[ :uname ], datesA )               # datesA could be empty

      else
         print( "\n*** %s matches but grid does not exist" % prefix + "pw" )
         out = write( prefix + "grid", "[]" )              # set missing grid to empty grid
         raise( "error writing empty grid" ) if out.nil?   # TODO: this is really 500
         gridA = [ ]
      end

      if settings.fwork == :jquery
         fwork  = "<script type='application/javascript' src='#{ settings.jquery }'></script>"
         fwork += "<script type='application/javascript' src='#{ settings.client }'></script>"
         start  = "<script type='application/javascript'>$( window ).on( 'load', on_load )</script>"
      elsif settings.fwork == :react
         fwork  = "<script type='application/javascript' src='#{ settings.react     }'></script>"
         fwork += "<script type='application/javascript' src='#{ settings.react_dom }'></script>"
         fwork += "<script type='application/javascript' src='#{ settings.babel     }'></script>"
         start  = "<script type='text/babel' src='#{ settings.client }'></script>"
      end

      css    = "<link rel='stylesheet' href='#{ settings.css }'>"
      var    = "<script type='application/javascript'>var grid=#{ gridA.to_json }</script>"
      client = "<script type='application/javascript' src='#{ settings.client }'></script>"

      head   = "<head>#{ fwork }#{ css }#{ var }</head>"
      body   = "<body><div id='grid'></div>#{ start }</body>"
      headers( "Content-Type" => "text/html" )

      [ 200, "<html>#{ head }#{ body }</html>" ]

   rescue RuntimeError => e   # raise() throws RuntimeError by default
      headers( "Content-Type" => "application/json" )
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------
# kludge so i don't have to learn mustermann

get( "#{ ENV[ "PHLUID_ROOT" ] }/*.js" ) {
   send_file( ENV[ "PHLUID_ROOT" ] + "/" + params[ :splat ].first + ".js" )
}

get( "#{ ENV[ "PHLUID_ROOT" ] }/*.css" ) {
   send_file( ENV[ "PHLUID_ROOT" ] + "/" + params[ :splat ].first + ".css" )
}

#-----------------------------------------------------------------------------------------------------------------------
# can a task ever be created without also being in a date? (future) yes, if there are task templates

post( "/task" ) {
   headers( "Content-Type" => "application/json" )

   begin
      raise( "no user logged in" ) if session[ :user ].nil?
      req = data_check( request.body.read, :post_task, :json )
      date_key  = "date:%s:%s" % [ session[ :user ], req[ "date" ] ]
      task_list = read( date_key )
      raise( "date record does not exist: " + date_key ) if task_list.nil?
      raise( "task_list does not match date record"    ) if req[ "task_list" ] != task_list         # comparing 2 arrays
      raise( "invalid position: " + position           ) if ! req[ "position" ].between?( 0, task_list.length )
      task_id = SecureRandom.uuid()
      task_list.insert( req[ "position" ].to_i, task_id )
      out = write( date_key, task_list )
      raise( "error writing task list" ) if out.nil?   # TODO: this is a 500
      req[ "new" ][ "links" ] = [ date_key ]
      out = write( task_id, req[ "new" ] )
      raise( "error writing new task" ) if out.nil?    # TODO: this is a 500
      [ 200, { status: "ok", task_id: task_id }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

get( "/task/:task_id" ) {
   headers( "Content-Type" => "application/json" )

   begin
      data_check( params[ :task_id ], :task_id )
      task = read( "task:" + params[ :task_id ] )
      raise( "task_id does not exist: " + params[ :task_id ] ) if task.nil?
      [ 200, { status: "ok", task: task }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

put( "/task/:task_id" ) {          # write existing task; error if task does not already exist
   headers( "Content-Type" => "application/json" )

   begin
      new_task = data_check( request.body.read, :task, :json )
      data_check( params[ :task_id ], :task_id )
      old_task = read( "task:" + params[ :task_id ] )
      raise( "task does not exist: " + params[ :task_id ] ) if old_task.nil?
      new_task[ "links" ] = old_task[ "links" ]             # links field in tasks sent from client not required and ignored
      out = write( params[ :task_id ], new_task )
      raise( "error on task write" ) if out.nil?            # TODO: this is a 500
      [ 200, { status: "ok" }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

put( "/task/move/:task_id" ) {          # move task from one date to another
   headers( "Content-Type" => "application/json" )

   begin
      raise( "user not logged in" ) if session[ :user ].nil?
      data_check( params[ :task_id ], :task_id )
      task = read( params[ :task_id ] )
      raise( "task_id does not exist: " + params[ :task_id ] ) if task.nil?           # does url:task_id exist?
      req  = data_check( request.body.read, :move_task, :json )                       # check request body format

      old_date_key  = "date:%s:%s" % [ session[ :user ], req[ "current" ] ]
      old_task_list = read( old_date_key )
      raise( "current date does not exist: " + old_date_key ) if old_task_list.nil?   # does req:current date exist?

      new_date_key  = "date:%s:%s" % [ session[ :user ], req[ "new" ] ]
      new_task_list = read( new_date_key )
      raise( "new date does not exist: " + new_date_key ) if new_task_list.nil?       # does req:new date exist?

      if ! old_task_list.include?( params[ :task_id ] )                 # is url:task_id in req:current date task list?
       raise( "task_id #{ params[ :task_id ] } not in current date task list" )
      end

      if new_task_list.include?( params[ :task_id ] )
         raise( "task_id #{ params[ :task_id ] } already exists in new date task list" )
      end

      if ! req[ "task_list" ].include?( params[ :task_id ] )            # is url:task_id in req:task_list?
         raise( "task_id #{ params[ :task_id ] } not in new date task list" )
      end

      # is req task_list equal to previous req new date task list plus url task_id?
      if req[ "task_list" ] - new_task_list != [ params[ :task_id ] ]
         raise( "task_list != new date task list plus task_id" )
      end

      write( old_date_key, old_task_list - [ params[ :task_id ] ] )   # remove url:task_id from req:current date task list
      write( new_date_key, req[ "task_list" ] )                       # write req:task_list into req:new date
                                                                      # TODO: check for write errors
      task[ "links" ] = task[ "links" ] - [ old_date_key ] + [ new_date_key ]
      write( params[ :task_id ], task )                               # update links for url:task_id
      [ 200, { status: "ok" }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

delete( "/task/:task_id" ) {   # params: date=Date (required)
   headers( "Content-Type" => "application/json" )

   begin
      raise( "user not logged in" ) if session[ :user ].nil?
      raise( "missing date parameter" ) if ! params.has_key?( "date" )
      data_check( params[ :date ], :date )
      task = read( "task:" + params[ :task_id ] )
      raise( "task_id does not exist: " + params[ :task_id ] ) if task.nil?
      date_key  = "date:%s:%s" % [ session[ :user ], params[ "date" ] ]
      task_list = read( date_key )
      raise( "date does not exist: " + date_key ) if task_list.nil?
      raise( "task_id #{ params[ :task_id ] } not found on date" ) if ! task_list.include?( params[ :task_id ] )
      raise( "date #{ date_key } not found in task links" ) if ! task[ "links" ].include?( date_key )   # really status 500
      write( date_key, task_list - [ params[ :task_id ] ] )   # remove url:task_id from url:date task list
      task[ "links" ] = task[ "links" ] - [ date_key ]        # remove url:date from url:task_id links

      if task[ "links" ].empty?
         print( "task #{ params[ :task_id ] } no longer appears in any date of any user" )
         settings.redis.del( params[ :task_id ] )             # if this was last date in which task appeared, delete task
      else
         write( params[ :task_id ], task )                    # ...else update task links
      end

      [ 200, { status: "ok" }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

post( "/date/:date" ) {
   headers( "Content-Type" => "application/json" )

   begin
      raise( "user not logged in" ) if session[ :user ].nil?
      #body = request.body.read
      #print( "*** post /date request body:#{ body }:L=#{ body.length }:C=#{ body.class }" )    ###### TEST
      dateD = data_check( params[ :date ], :date )
      date_key  = "date:%s:%s" % [ session[ :user ], params[ "date" ] ]
      task_list = read( date_key )
      raise( "date already exists: " + date_key ) if ! task_list.nil?
      task_list = data_check( request.body.read, :task_list, :json )

      task_list.each { | task_id |     # add date to links for all tasks in task list
         task = read( "task:" + task_id )
         raise( "task_id does not exist: " + task_id ) if task.nil?
         task[ "links" ] << date_key
         write( task_id, task )        # TODO: check for write error
      }

      write( date_key, task_list )
      grid_key = "user:%s:grid" % session[ :user ]
      gridA = read( grid_key )
      raise( "grid not found: " + grid_key ) if gridA.nil?   # existence of grid key was checked at login

      if ! gridA.empty?
         earliestD = Date.strptime( gridA[ 0 ][ 0 ], DATE_P )

         if dateD < earliestD          # params[ :date ] earlier than first date in gridA
            gridA = insert_before( gridA, params[ :date ], dateD, earliestD )

         else                          # params[ :date ] later than first date in gridA
            gridA.reverse_each.with_index { | rangeA, i |
               prev_dateD = Date.strptime( rangeA[ -1 ], DATE_P )

               if dateD > prev_dateD   # insert after gridA[i]
                  gridA = insert_after( gridA, params[ :date ], dateD, prev_dateD, gridA.length - 1 - i, i == 0 )
                  break                # insert_after modifies gridA but loop does not continue
               end
            }
         end

      else                             # gridA is empty
         gridA = [ [ params[ :date ] ] ]
      end

      write( grid_key, gridA )
      [ 200, { status: "ok" }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

get( "/date/:date" ) {   # params: end=Date (optional)
   headers( "Content-Type" => "application/json" )

   begin
      raise( "user not logged in" ) if session[ :user ].nil?
      startD = data_check( params[ :date ], :date )            # data_check() returns Date instance for :date type

      if params.has_key?( :end )
         stopD = data_check( params[ :end ], :date )
      else
         stopD = startD
      end

      body = { status: "ok" }

      startD.step( stopD ) { | dateD |
         dateS     = dateD.strftime( DATE_P )
         date_key  = "date:%s:%s" % [ session[ :user ], dateS ]
         task_list = read( date_key )
         raise( "date does not exist: " + date_key ) if task_list.nil?
         body[ dateS ] = task_list
      }

      [ 200, body.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

put( "/date/:date" ) {
   headers( "Content-Type" => "application/json" )

   begin
      raise( "user not logged in" ) if session[ :user ].nil?
      data_check( params[ :date ], :date )
      task_list = data_check( request.body.read, :task_list, :json )
      date_key  = "_#{ session[ :user ] }:#{ params[ :date ] }"
      raise( "date does not exist: " + date_key ) if ! settings.redis.exists?( date_key )
      write( date_key, task_list )      # TODO: check write result
      [ 200, { status: "ok" }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

delete( "/date/:date" ) {
   headers( "Content-Type" => "application/json" )

   begin
      raise( "user not logged in" ) if session[ :user ].nil?
      data_check( params[ :date ], :date )
      date_key = "_#{ session[ :user ] }:#{ params[ :date ] }"
      raise( "date does not exist: " + date_key ) if ! settings.redis.exists?( date_key )
      out = settings.redis.del( date_key )
      raise( "error deleting key: " + date_key ) if out != 1
      [ 200, { status: "ok" }.to_json ]

   rescue RuntimeError => e
      [ 400, { status: "error", msg: e.message }.to_json ]
   end
}

#-----------------------------------------------------------------------------------------------------------------------

put( "/logout" ) {          # TODO: user-initiated session termination
   settings.redis.save
   settings.redis.shutdown
   print( "\n*** redis saved and shut down" )
   Sinatra::Application.quit!
}

#-----------------------------------------------------------------------------------------------------------------------

get( "*" ) {
   msg = "invalid GET: " + params[ :splat ].first
   print( "\n*** ", msg )
   [ 400, { status: "error", msg: msg }.to_json ]
}

put( "*" ) {
   msg = "invalid PUT: " + params[ :splat ].first
   print( "\n*** ", msg )
   [ 400, { status: "error", msg: msg }.to_json ]
}

post( "*" ) {
   msg = "invalid POST: " + params[ :splat ].first
   print( "\n*** ", msg )
   [ 400, { status: "error", msg: msg }.to_json ]
}

delete( "*" ) {
   msg = "invalid DELETE: " + params[ :splat ].first
   print( "\n*** ", msg )
   [ 400, { status: "error", msg: msg }.to_json ]
}

#-----------------------------------------------------------------------------------------------------------------------

run!   # run the webserver; needed because this is not a "classic style" sinatra app (set( :run, true ) did not work)
       # in classic style sinatra apps, routes appear standalone in the file, not inside a class definition
       # routes need to be inside a class definition due to other classes being defined in this file

end    # class Phluid

########################################################################################################################
