
## directory layout

- server.rb   : server source
- dump.rdb    : redis database binary data
- populate.rb : utility to populate redis database with test data
- test.rb     : code to test behavior of various alternatives

## installation

install the following packages using "apt install [package name]" (perhaps preceded by "sudo"):
- ruby (version 2.7 or later - although it's probably the case that ruby 3.0 or later is needed)
- redis

after ruby is installed, install the following gems using "gem install [gem name]" to insall the latest version or "gem install [gem name] --version [version]" to install a specific version. these commands may need to be preceded by "sudo".
- sinatra
- puma 
- redis
- haml (version 5.2.2 - the current server source code does not support haml 6.x)
- securerandom

## run

at the command line, enter "ruby server.rb"

sinatra will launch the puma web server and listen on localhost:4567.
the port number can be changed by passing options to sinatra - see sinatra documentation for details.
