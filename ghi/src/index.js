import React, { Children } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { Provider } from "react-redux";
import { App } from "./App"
import { store } from './app/store'
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { TaskList } from './Components/Tasks/taskList';
import { CreateTaskForm } from './Components/Tasks/createTaskForm';
import { Calendar } from './Components/Tasks/Calendar';
import { LoginForm } from './Components/Account/Login';
import { SignUpForm } from './Components/Account/Signup';


const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");


const router = createBrowserRouter([
  {
    element: <App />,
    errorElement: "weeeeeeeeewoooooooo",
    children: [
        {
          path: "/tasks",
          element: <TaskList/>
        },
        {
          path: "/tasks/add",
          element: <CreateTaskForm/>
        },
        {
          path: "/tasks/dashboard",
          element: <Calendar />
        },
        {
          path: "/login",
          element: <LoginForm />
        },
        {
          path: '/signup',
          element: <SignUpForm />
        },
    ]
  }
], [basename])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
