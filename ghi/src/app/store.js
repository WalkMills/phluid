import { configureStore} from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { phluidApi } from './apiSlice'


export const store = configureStore ({
    reducer: {
        [phluidApi.reducerPath]: phluidApi.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(phluidApi.middleware)
})

setupListeners(store.dispatch)