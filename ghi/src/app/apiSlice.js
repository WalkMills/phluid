import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const phluidApi = createApi({
    reducerPath: "phluidApi",
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST
    }),
    endpoints: (builder) => ({
        getAllTasks: builder.query({
            query: () => ({
                url: "/api/tasks",
                credentials: 'include'
            }),
            transformResponse: (response) => response.tasks,
            providesTags: ["Tasks"]
        }),
        getOneTask: builder.query({
            query: (task_id) => ({
                url: `/api/tasks/${task_id}`,
                method: "GET",
                credentials: 'include'
            }),
            providesTags: ["Tasks"]
        }),
        updateTask: builder.mutation({
            query: ({task_id, body}) => ({
                url: `/api/tasks/${task_id}`,
                body: body,
                method: 'PUT',
                credentials: 'include'
            }),
            invalidatesTags: ["Tasks"]
        }),
        deleteTask: builder.mutation({
            query: (task_id) => ({
                url: `/api/tasks/${task_id}`,
                method: 'DELETE',
                credentials: "include"
            }),
            invalidatesTags: ["Tasks"]
        }),
        createTask: builder.mutation({
            query: (task) => ({
                url: '/api/tasks',
                body: task,
                method: 'POST',
                credentials: 'include'
            }),
            invalidatesTags: ["Tasks"]
        }),
       logout: builder.mutation({
            query: () => ({
                url: "/token",
                method: "DELETE",
                credentials: "include",
            }),
            invalidatesTags: ["Account", "Tasks"],
        }),
        login: builder.mutation({
            query: ({ username, password }) => {
                const body = new FormData();
                body.append("username", username);
                body.append("password", password);
                return {
                    url: `/token`,
                    method: `POST`,
                    body,
                    credentials: "include",
                };
            },
            invalidatesTags: ["Account", "Tasks"],
        }),
        signup: builder.mutation({
            query: (body) => ({
              url: `/api/accounts`,
              method: "POST",
               body,
               credentials: "include",
           }),
            invalidatesTags: ["Account", "Tasks"],
        }),
        getAccount: builder.query({
            query: () => ({
             url: "/token",
            credentials: "include",
        }),
        transformResponse: (response) => (response ? response.account : null),
        providesTags: ["Account"],
    }),
    })
})

export const {
    useGetAllTasksQuery,
    useGetOneTaskQuery,
    useUpdateTaskMutation,
    useDeleteTaskMutation,
    useCreateTaskMutation,
    useGetAccountQuery,
    useLoginMutation,
    useLogoutMutation,
    useSignupMutation
} = phluidApi