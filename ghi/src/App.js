import { Outlet } from "react-router-dom";
import { Nav } from "./Components/Nav";


export const App = () => {
  return (
    <div>
      <style>
      </style>
      <Nav />
      <div>
        <Outlet />
      </div>
      <div className="App">
      </div>
    </div>
  );
}