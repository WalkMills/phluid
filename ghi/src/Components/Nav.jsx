import { NavLink, useNavigate } from "react-router-dom";
import { useGetAccountQuery, useLogoutMutation } from "../app/apiSlice";
import './Nav.css'

export const Nav = () => {
 
    const navigate = useNavigate()
    const {data: account} = useGetAccountQuery()
    const [logout] = useLogoutMutation()

    const handleLogout = () => {
        logout()
        navigate("/login")
    }

    return (
        <nav className="nav-style">
            <div>
                <div>
                    <ul className="nav-style">
                        {account && <li className="list-style">
                            <NavLink to={"/tasks"}>Tasks</NavLink>
                            </li>}
                        {!account && <li className="list-style">
                            <NavLink to={"/login"}>Login</NavLink>
                            </li>}
                        {!account && <li className="list-style">
                            <NavLink to={'/signup'}>Sign Up</NavLink>
                            </li>}
                        {account && <button className="list-style" onClick={handleLogout}>
                            Logout
                            </button>}
                    </ul>
                </div>
            </div>
        </nav>


    )

}