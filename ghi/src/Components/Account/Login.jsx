import { useLoginMutation } from "../../app/apiSlice";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";

export const LoginForm = () => {
    const [login] = useLoginMutation()
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        'username': '',
        'password': ''
    })

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (login(formData)) {
            setFormData({
            'username': '',
            'password': ''
            })
            document.getElementById('login-form').reset()
            navigate('/tasks')
        }
    }

    return (
        <div>
            <div>
                <div>
                    <h2>Login</h2>
                    <form id="login-form" onSubmit={handleSubmit}>
                        <div>
                            <label htmlFor="username">Username</label>
                            <input id="username" name="username" required type="text" onChange={handleChange} placeholder="username"/>
                        </div>
                        <div>
                            <label htmlFor="password">Password</label>
                            <input id="password" name="password" type="text" placeholder="password" onChange={handleChange} required />
                        </div>
                        <button type="submit">Login</button> or <button type="button" onClick={() => navigate('/signup')}>Sign Up</button>
                    </form>
                </div>
            </div>
        </div>
    )
}