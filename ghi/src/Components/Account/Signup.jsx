import { useSignupMutation } from "../../app/apiSlice";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export const SignUpForm = () => {
    const [signUp] = useSignupMutation()
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        'username': '',
        'password': ''
    })

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        signUp(formData)
        setFormData({
        'username': '',
        'password': ''
        })
        navigate('/tasks')        
    }

    return (
        <div>
            <div>
                <div>
                    <h2>Sign Up</h2>
                    <form id="signUp-form" onSubmit={handleSubmit}>
                        <div>
                            <label htmlFor="username">Username</label>
                            <input id="username" name="username" required type="text" onChange={handleChange} placeholder="username"/>
                        </div>
                        <div>
                            <label htmlFor="password">Password</label>
                            <input id="password" name="password" type="text" placeholder="password" onChange={handleChange} required />
                        </div>
                        <button type="submit">Sign Up</button> or <button type="button" onClick={() => navigate('/login')}>Login</button>
                    </form>
                </div>
            </div>
        </div>
    )
}