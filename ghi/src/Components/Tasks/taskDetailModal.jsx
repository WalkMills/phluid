
function TaskDetailModal({task, description, status, show, onClose, due_date}) {
    iq

    return (    
        <div className="modal">
            <div className="modal-content">
                <div className="modal-header">
                    <h4 className="modal-title">{task}</h4>
                </div>
                <div className="modal-body">
                    Status: {status}
                    <br/>
                    Description: {description}
                    <br/>
                    Due Date: {due_date}
                </div>
                <div className="modal-footer">
                    <button className="modal-button" onClick={onClose}>Close</button>
                </div>
            </div>
        </div>
    )
}