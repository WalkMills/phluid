import { useState } from "react";
import { useEffect } from "react";
import './taskBlock.css'
import { TaskDetailModal } from "./taskDetailModal";

export const TaskBlock = ({task}) => {
    const [showModal, setShowModal] = useState(false)
    const statusArray = ['rgb(247, 150, 150)', 'rgb(250,250,170)', 'rgb(189,250,170)' ]
    const [backgroundColor, setBackgroundColor] = useState('')


    const loadBackgroundColor = () => {
        if (task.status === "to do") {
            setBackgroundColor(statusArray[0])
        } else if (task.status === "doing") {
            setBackgroundColor(statusArray[1])
        } else {
            setBackgroundColor(statusArray[2])
        }

    }

    useEffect(() => {
        loadBackgroundColor()
    }, [])

    const handleClick = (event) => {

        if (event.shiftKey) {
            setShowModal(true)
        } else {
            if (backgroundColor === statusArray[0]) {
                setBackgroundColor(statusArray[1])
            } else if (backgroundColor === statusArray[1]) {
                setBackgroundColor(statusArray[2])
            } else {
                setBackgroundColor(statusArray[0])
            }
        }
    }

    return (
        <>
            <div
            className="task-div"
            onClick={handleClick}
            style={{'backgroundColor': backgroundColor}}
            >
                {task.task}
            </div>
            <TaskDetailModal task={task.task} status={task.status} description={task.description} due_date={task.due_date} show={showModal} onClose={() => setShowModal(false)}/>
        </>
    )
}