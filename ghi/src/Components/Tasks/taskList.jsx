import { useGetAllTasksQuery, useDeleteTaskMutation } from "../../app/apiSlice";
import { UpdateTaskButtons } from "./updateTaskButtons";
import { useNavigate } from 'react-router-dom'


export const TaskList = () => {
    const {data, isLoading} = useGetAllTasksQuery()
    const [deleteTask] = useDeleteTaskMutation()
    const navigate = useNavigate()



    if (isLoading) return <div>Loaidng...</div>

    return (
        <div>
            <div>
                <div>
                    <h2>Tasks</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>Task</th>
                                <th>Due Date</th>
                                <th>Completed</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((task) => {
                                return (
                                    <tr key={task.id}>
                                        <td>{task.task}</td>
                                        <td>{task.due_date}</td>
                                        <td>
                                            {<UpdateTaskButtons task={task}/>}
                                        </td>
                                        <td><button onClick={() => deleteTask(task.id)}>Remove</button></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    <button onClick={() => navigate('/tasks/add')}>Add Task</button>
                </div>
            </div>
        </div>
    )
}