import { useUpdateTaskMutation } from "../../app/apiSlice";


export const UpdateTaskButtons = ({task}) => {
    const [updateTask] = useUpdateTaskMutation()


    const handleUpdate = () => {
        const body = {
                'task': task.task,
                'due_date': task.due_date,
                'completed': true
            }
        const task_id = task.id
        updateTask({task_id, body})
    }

    // const handleUpdate = () => {
    //     const options = {
    //         method: "PUT",
    //         header: { 'Content-Type': 'application/json' },
    //         body: {
    //             'task': task.task,
    //             'due_date': task.due_date,
    //             'completed': true
    //         }
    //     }
    //     const url = `https://localhost:8000/api/tasks/${task.id}`
    //     fetch(url, options)
    // }

    return (
        <>
            {task.completed ? <input type="checkbox" defaultChecked/> : <input onChange={handleUpdate} type="checkbox" />}
        </>
    )

}