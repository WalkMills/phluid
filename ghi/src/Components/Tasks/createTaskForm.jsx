import { useState } from "react";
import { useCreateTaskMutation } from "../../app/apiSlice";
import { useNavigate } from 'react-router-dom'


export const CreateTaskForm = () => {
    const navigate = useNavigate()
    const [task] = useCreateTaskMutation()
    const [formData, setFormData] = useState({
        'task': '',
        'due_date': '',
        'completed': "to do",
        'description': '',
        'date_created': new Date(Date.now()).toISOString().slice(0, 10)
    })

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        task(formData)
        setFormData({
        'task': '',
        'due_date': '',
        'completed': "to do"
        })
        navigate('/tasks')
        // document.getElementById('add-task-form').reset()
    }

    return (
        <div>
            <div>
                <div>
                    <h2>Create Task</h2>
                    <form onSubmit={handleSubmit} id="add-task-form">
                        <div>
                            <label htmlFor="task">Task</label>
                            <input required name="task" id="task" type="text" placeholder="task" onChange={handleChange}/>
                        </div>
                        <div>
                            <label htmlFor="due_date">Due Date</label>
                            <input required name="due_date" id="due_date" type="date" placeholder="due_date" onChange={handleChange}/>
                        </div>
                        <div>
                            <label htmlFor="description">Description</label>
                            <textarea required name="description" id="description" rows={4} cols={50} onChange={handleChange} />
                        </div>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    )
}