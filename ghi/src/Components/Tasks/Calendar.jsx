import { useGetAllTasksQuery } from "../../app/apiSlice";
import "./calendar.css"
import { useState } from "react";
import { TaskBlock } from "./taskBlock";

export const Calendar = () => {
    const {data, isLoading} = useGetAllTasksQuery()
    const [showModal, setShowModal] = useState(false)
    
    const handleViewChange = () => {
        
    }
    
    let threeDayView = [
        [-1],
        [0],
        [1]
    ]
    
    let weekView = [
        [-3],
        [-2],
        [-1],
        [0],
        [1],
        [2],
        [3]
    ]
    
    threeDayView = threeDayView.map(slot => {
        return (
            slot.map(num => {
                let day = new Date(Date.now())
                day.setDate(day.getDate() + num)
                return new Date(day).toISOString().slice(0, 10)
            })
            )
        })
        
        weekView = weekView.map(slot => {
            return (
                slot.map(num => {
                    let day = new Date(Date.now())
                    day.setDate(day.getDate() + num)
                    return new Date(day).toISOString().slice(0, 10)
                })
                )
            })
            
        const [view, setView] = useState(threeDayView)
        if (isLoading) return <div>Loading...</div>
        return (

        <>


                <ul className="list">
                    <li className="list-item">View :</li>
                    <li className="list-item" id="button"><button onClick={() => setView(threeDayView)}>3 Day</button></li>
                    <li className="list-item" id="button"><button onClick={() => setView(weekView)}>Week</button></li>
                </ul>
                <div className="table-container">
                        {view.map(date => {
                            return (
                            
                                <div className="cell">
                                    <div className="table-row">
                                        <div className="table-header">{date}</div>
                                            {data.map(task => {
                                                return (
                                                    <>
                                                    { task.date_created == date ? <TaskBlock task={task}/> : null}
                                                    </>
                                                )
                                            })}
                                            <button className="add-button">+</button>
                                        </div>
                                </div>
                            )
                        })}
                </div>
        </>

    )
}