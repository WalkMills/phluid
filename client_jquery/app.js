
//######################################################################################################################
//----------------------------------------------------------------------------------------------------------------------
   //-------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------

function task_click( e ) {
   if ( e.button != 0 ) return false     // main button (usually left) not clicked; "main" depends on mouse driver config
   // display popup( ; save, cancel )
   return false
}

//----------------------------------------------------------------------------------------------------------------------

function task_dblclick( e ) {
   if ( e.button != 0 ) return false     // main button (usually left) not clicked; "main" depends on mouse driver config
   // display popup( ; save, cancel )
   return false
}

//----------------------------------------------------------------------------------------------------------------------
var task_count = 0
var statusA = [ "todo", "doing", "done" ]

function task_add_click( e ) {           // single click
   if ( e.button != 0 ) return false     // main button (usually left) not clicked; "main" depends on mouse driver config

   // display dialog

   task_count += 1
   const index = task_count % statusA.length

   const taskO = {                       // create fake task in until dialog working; should be filled in from dialog field values
      title:    "title #" + task_count.toString(),
      status:   statusA[ index ],
      contents: "contents of title #" + task_count.toString(),
      type:     "text",
   }
   const dayE = $( e.target ).parent()   // day element is parent of task_add
   const task_list = dayE
      .children( "[ data-task_id ]" )    // ...and it contains all tasks
      .map( ( index, el ) => { return $( el ).attr( "data-task_id" ) } )
      .toArray()
   const body = {
      date:      dayE.attr( "id" ),      // ...and day element id is date
      task_list: task_list,
      new:       taskO,
      position:  task_list.length        // task_add adds new task at end of current task list
   }

   fetch( "/task", {                     // tell server about the new task
      method:  "POST",
      body:    JSON.stringify( body ),
      headers: { "Content-Type": "application/json" }
   } )

   .then( ( response  ) => response.json() )   // wait for entire body to arrive and convert from json to js type

   .then( ( resp_body ) => {                   // wait for javascript type to be available

      if ( resp_body.status == "ok" ) {
         taskO[ "id" ] = resp_body.task_id              // task_id is created by server; add it to task object...
         $( e.target ).before( task_create( taskO ) )   // ...then

      } else {                                          // server did not reply with status ok
         console.log( "post /task" )
         console.log( "status:", resp_body.status )
         console.log( "body:", resp_body )
      }
   } )

   return false
}

//----------------------------------------------------------------------------------------------------------------------

function day_add_click( e ) {           // single click
   if ( e.button != 0 ) return false    // main button (usually left) not clicked; "main" depends on mouse driver config

   const latest_dateS = $( e.target )   // click target was day_add element which is last child of #grid...
      .prev()                           // ...whose immediately preceding sibling is latest day element
      .children( ".date" )              // ...that contains a date element...
      .text()                           // ...that contains the lastest date text

   const latest_dateD = new Date( latest_dateS )
   latest_dateD.setUTCDate( latest_dateD.getUTCDate() + 1 )       // day_add adds sequentially-next date
   const next_dateS = latest_dateD.toISOString().slice( 0, 10 )
   const dayE = day_create( next_dateS, [ ] )                     // currently new dates are created with no tasks

   const url = "/date/" + next_dateS
   fetch( url, {                                // tell server about the new date
      method:  "POST",
      body:    "[ ]",
      headers: { "Content-Type": "application/json" }
   } )

   .then( ( response  ) => response.json() )    // wait for entire body to arrive and convert from json to js type

   .then( ( resp_body ) => {                    // wait for javascript type to be available

      if ( resp_body.status == "ok" ) {
         $( e.target ).before( dayE )           // add new day element before day_add element

      } else {                                  // server did not reply with status ok
         console.log( "post", url )
         console.log( "status:", resp_body.status )
         console.log( "body:", resp_body )
      }
   } )

   return false   // stop bubbling?
}

//----------------------------------------------------------------------------------------------------------------------

function date_click( e ) {
   if ( e.button != 1 ) return false     // right click?
   // display dialog asking for confirmation to clear all tasks
   return false   // stop bubbling?
}

//----------------------------------------------------------------------------------------------------------------------

function task_create( taskO ) {
   const taskE = $( "<div>" )
      .attr( "class", "task " + taskO.status )
      .attr( "data-task_id", taskO.id )
      .text( taskO.title )
      .on( "click",    task_click )
      .on( "dblclick", task_dblclick )

   return taskE
}

//----------------------------------------------------------------------------------------------------------------------

function day_create( dateS, taskA ) {
   const dayE = $( "<div>" )               // create a day element, which contains...
      .attr( "id",    dateS )
      .attr( "class", "day" )

   const dateE = $( "<div>" )              // ...a date...
      .attr( "class", "date" )
      .text( dateS )
      .on( "click", date_click )
   dayE.append( dateE )

   for ( const task of taskA ) {           // ...followed by zero or more tasks...
      dayE.append( task_create( task ) )
   }

   const task_addE = $( "<div>" )          // ...followed by a task_add element
      .attr( "class", "task task-add" )
      .text( "+" )
      .on( "click", task_add_click )
   dayE.append( task_addE )

   return dayE
}

//----------------------------------------------------------------------------------------------------------------------

function on_load( e ) {                         // this handler assigned by code in <body><script>

   for ( const [ dateS, taskA ] of grid ) {     // "grid" global variable defined in <head><script>
      const dayE = day_create( dateS, taskA )
      $( "#grid" ).append( dayE )               // #grid contains zero or more day elements...
   }

   const day_addE = $( "<div>" )                // ...followed by a day_add element
      .attr( "class", "date date-add" )
      .text( "+" )
      .on( "click", day_add_click )
   $( "#grid" ).append( day_addE )

   return false                                 // stop bubbling?
}

//----------------------------------------------------------------------------------------------------------------------
