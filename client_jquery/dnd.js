// when are modifications to channels written
//// when another channel is selected and on unload? save button click? every X seconds?

// consider creating new channel popup once on load then change its visibility (and any other popups?)
//// need to clear fields when hiding (or displaying)

// does quill supply its own buttons and handlers?

//######################################################################################################################
//----------------------------------------------------------------------------------------------------------------------
   //-------------------------------------------------------------------------------------------------------------------

//######################################################################################################################
// global variables

var ch_menu = $( "<div><div>add above</div><div>add below</div><div>rename</div><div>delete</div></div>" )
// TODO: add class and id to each menu item

//----------------------------------------------------------------------------------------------------------------------

function track( event, e ) {   // print debugging state to console
   const eO = e.originalEvent

   if ( e.target !== eO.target ) {
      console.log( "%s: e target != eO target", event )
   }

   if ( e.currentTarget !== eO.currentTarget ) {
      console.log( "%s: e currentTarget != eO currenTarget", event )
   }

   const boxT  = e.target.getBoundingClientRect()                // returns viewport coordinates
   const boxCT = e.currentTarget.getBoundingClientRect()
   console.log( "---------------------" )
   console.log( "%s: CTid: %s  Tid: %s", event, e.currentTarget.id, e.target.id )
   console.log( "CTbot %4d  CTtop %4d", boxCT.bottom, boxCT.top )
   console.log( " Tbot %4d   Ttop %4d", boxT.bottom,  boxT.top  )
   console.log( " WinY %4d  PageY %4d   excObjY %4d", window.scrollY, e.pageY, e.y )
}

//----------------------------------------------------------------------------------------------------------------------

function drag_start( e ) {
   const e_dt = e.originalEvent.dataTransfer       // jquery wraps browser event object in its own object
   e_dt.setData( "source_id", e.target.id )        // TODO: remember entire html element instead?

   if ( e.button == 2 ) {                          // right click; e.button is different than e.buttons
      e_dt.dropEffect = "copy"                     // TODO: use shift-leftclick instead?
   } else {
      e_dt.dropEffect = "move"                     // move is default action; e.button==0 is left click
   }

   guide = [ ]                                     // capture screen coordinates of channel elements at start of dnd

   $( "#guide" ).children().each( ( i, el ) => {   // el refers to child dom elements
      const box     = el.getBoundingClientRect()   // in chrome and ff "this" refers to #guide on all iterations
      const channel = {
         id:  el.id,
         mid: ( box.top + box.bottom ) / 2 + window.scrollY   // element midpoint; assumes no scroll while dragging
      }
      guide.push( channel )
      return true                                  // guarantee each() loop does not exit early
   } )

   e_dt.setData( "guide", JSON.stringify( guide ) )   // dataTransfer property apparently only stores strings

   //console.log( "guide: ", guide )
   //track( "start", e )
}

//----------------------------------------------------------------------------------------------------------------------

function skip( e ) {   // called on dragenter, dragover, and dragleave
   return false        // causes preventDefault() and stopPropagation()
}

//----------------------------------------------------------------------------------------------------------------------

function drop( e ) {
   const e_dt      = e.originalEvent.dataTransfer          // jquery wraps browser event object in its own object
   const source_id = e_dt.getData( "source_id" )
   const guide     = JSON.parse( e_dt.getData( "guide" ) )
   const channel   = guide.find( ( el ) => e.pageY < el[ "mid" ] )   // find first channel element w/midpoint below,
                                                                     // Y coordinate increases going down screen i.e. higher on the screen, than drop point
   //console.log( "drop source id: ", source_id )
   //console.log( "found: ", channel )

   if ( channel != undefined ) {                           // element dropped above midpoint of lowest element
      if ( source_id != channel[ "id" ] ) {                // was element dragged far enough to require moving?
         const source_el = $( "#" + source_id ).detach()   // remove html element from dom and return it
         $( "#" + channel[ "id" ] ).before( source_el )    // insert source_el before receiver
      }

   } else {                                                // element dropped below midpoint of lowest element
      const last_id = guide[ guide.length - 1 ][ "id" ]

      if ( source_id != last_id ) {                        // are we not dragging the lowest element?
         const source_el = $( "#" + source_id ).detach()
         $( "#" + last_id ).after( source_el )             // insert source_el after receiver
      }
   }

   //track( "drop", e )
   return false   // causes preventDefault() and stopPropagation()
}

//----------------------------------------------------------------------------------------------------------------------

function channel_mdown( e ) {      // mousedown on channel element within guide
   console.log( "activated channel_mdown()" )
   if ( e.button == 0 ) {          // left button; make selected channel active and display in #content
      // body = get( "/channel/#{ e.target.data-ch-type }/#{ e.target.id }?active" )
      // remove/detach existing child of #content, if any; TODO: automatically save or ask to save?
      // put( "/channel/#{ $( #content ).attr( "data-ch-type" ) }/#{ $( "#content" ).attr( "id") }" )  // TODO: is this the child of #content?
      // $( "#content" ).detach()   // TODO: just detach the child of #content?
      // $( "#content" ).addChild( body )

   } else {                        // default is display menu; e.button==2 is rightclick
      const box    = e.target.getBoundingClientRect()
      const guide  = $( "#guide" ).getBoundingClientRect()
      const styles = {
         position:  "absolute",
         "z-index": 1,
         left:      guide.left + "px",   // TODO: need to convert left to string?
         top:       box.top + "px"
      }
      $( "body" ).append( ch_menu ).css( styles ).on( "mousedown", menu_mdown )   // ch_menu is a global var
      // assuming element origin is upper left corner:
      // menuY = Y of top edge of clicked channel
      // menuX = X of rhs of clicked channel + margin to guide (rhs of #guide?)
      // menuZ = in front of everything
   }
}

//----------------------------------------------------------------------------------------------------------------------

function menu_mdown( e ) {    // mousedown on menu item
   console.log( "activated menu_mdown()" )
   // each menu item has its own id
   // switch based on id of clicked element
   // remove/detach menu element
}

//----------------------------------------------------------------------------------------------------------------------
// this handler for load event was assigned in haml

function load( e ) {   // assign event handlers after dom is loaded
   $( "#guide" )
      .on( "dragstart", drag_start )
      .on( "dragenter", skip )
      .on( "dragover",  skip )
      .on( "dragleave", skip )
      .on( "drop",      drop )

   $( ".channel" ).on( "mousedown", channel_mdown )
}
