# this image contains code needed for phluid server.rb to run and defines environment variables.
# when executed, it creates a user and a working directory, and bind-mounts the git repo to the working directory.

FROM ubuntu:22.04

# RUN commands execute as root

RUN apt-get update &&                             \
    apt-get install --no-install-recommends --yes \
       ruby3.0                                    \
       redis-server

# remove apt package lists to decrease image size
RUN rm -rf /var/lib/apt/lists/*

RUN gem install sinatra -v 3.1.0
RUN gem install redis   -v 5.0.8
RUN gem install haml    -v 5.2.2

# cannot bind mount git repo to / so create a lower directory and bind there
RUN mkdir /phluid
WORKDIR /phluid
# server.rb uses this env var
ENV PHLUID_ROOT=/phluid

# create and run as a user (phluid) to limit priveleges
RUN useradd -ms /bin/bash phluid
USER phluid

# options on docker run command line are appended to this command
ENTRYPOINT [ "ruby", "server_ruby/server.rb" ]
