#!/bin/bash

pull="false"
build="false"
args=""

for arg in "$@"; do

   if [[ $arg == "-pull" ]]; then
      pull="true"
   elif [[ $arg == "-build" ]]; then
      build="true"
   else
      args+=" $arg"
   fi
done

if [[ $pull == "true" ]]; then
   $( git pull )
fi

if [[ $build == "true" ]]; then
   $( docker build --tag phluid-exec . )
fi

run="docker run -t --mount type=bind,src=.,dst=/phluid --network host phluid-exec $args"
$run
