
//-------------------------------------------------

function Grid( grid ) {
   prev_date = new Date( grid[ 0 ][ 0 ] )   // so no date gap on first loop iteration
   days = [ ]

   for ( i = 0; i < grid.length; i++ ) {
      [ date, taskA ] = grid[ i ]               // grid[ i ] = [ dateS, [ taskO, taskO,... ] ]
      dateD = new Date( date )

      if ( dateD > prev_date + 1 ) {                     // gap between previous date and current date; emit Date components to fill gap
         for ( d = prev_date + 1; d < dateD; d++ ) {     // d is a Date object
            dS = d.toString()
            days.push( <Day { key = dS, dS, [ ] } /> )   // day with empty task list
         }
      }

      days.push( <Day { key = date, date, taskA } /> )   // can key be accessed in Day? if so, then no need to pass date as a separate prop
      prev_date = dateD
   }

   return(         // this is inserted into the <div id="grid"> root element
      <>
         { days }
         <DateAdd />
      </>
   )
}

//-------------------------------------------------

function Day( date, taskA ) {   // is 'key' needed here?
   return(
      <Date { date, taskA.length == 0 } />
      { taskA.map( t => <Task { key = t.id, t } /> ) }
      <TaskAdd />
   )
}

//-------------------------------------------------

function Date( date, empty ) {
   className = "date" + empty && " emtpy"
   return( <div className={ className }>{ date }</div> )    // need to format date per wireframes
}

//-------------------------------------------------

function DateAdd( ) {
   return( <div className="date add">+</div> )
}

//-------------------------------------------------

function Task( task ) {   // is 'key' needed here?
   className = "task " + task.status
   return( <div id={ task.id } className={ className }>{ task.title }</div> )
}

//-------------------------------------------------

function TaskAdd( ) {
   return( <div className="task add">+</div> )
}

//-------------------------------------------------

grid = [
   ["2023-11-01", [
      {"title":"wipe your butt","type":"text","status":"todo","contents":"task_id 808: contents of wipe your butt","links":[],"id":"808"},
      {"title":"wash your underwear","type":"text","status":"done","contents":"task_id 909: contents of wash your underwear","links":[],"id":"909"},
      {"title":"leave a shit","type":"text","status":"doing","contents":"task_id 505: contents of leave a shit","links":[],"id":"505"} ] ],

   ["2023-11-03", [
      {"title":"leave a shit","type":"text","status":"doing","contents":"task_id 505: contents of leave a shit","links":[],"id":"505"} ] ],

   ["2023-11-04", [
      {"title":"wash your underwear","type":"text","status":"done","contents":"task_id 909: contents of wash your underwear","links":[],"id":"909"},
      {"title":"take a shit","type":"text","status":"doing","contents":"task_id 303: contents of take a shit","links":[],"id":"303"} ] ],
]
