import { useState } from "react";


export const CreateTaskForm = ({show, onClose}) => {

    if (!show) return null

    const [formData, setFormData] = useState({
        'title': '',
        'type':  '',       
        'status':   '',   
        'contents': '', 
    })

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault()


        fetch(
            'http://localhost:4567/task'
        )
    }

    return (    
        <div className="modal">
            <div className="modal-content">
                <div className="modal-header">
                    <h4 className="modal-title">Create Task</h4>
                </div>
                <div className="modal-body">
                    <form>
                        <div>
                            <label htmlFor="title">Title</label>
                            <input required id="title" name="title" onChange={handleChange}/>
                        </div>
                        <div>
                            <label htmlFor="type">Type</label>
                            <input required id="type" name="type" onChange={handleChange}/>
                        </div>
                        <div>
                            <label htmlFor="status">Status</label>
                            <input required id="status" name="status" onChange={handleChange}/>
                        </div>
                        <div>
                            <label htmlFor="contents">Contents</label>
                            <input required id="contents" name="contents" onChange={handleChange}/>
                        </div>
                    </form>
                </div>
                <div className="modal-footer">
                    <button className="modal-button" onClick={onClose}>Close</button>
                </div>
            </div>
        </div>
    )
}