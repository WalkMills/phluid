import { useState } from "react";


export const Login = () => {
    const [formData, setFormData] = useState({
        "uname": '',
        "pw": ''
    })

    const handleChange = (event) => {
        setFormData({
            ...formData, 
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = (event) => {

    }

    return (
        <div>
            <div>
                <h2>Login</h2>
                <div>
                    <form>          
                        <div>
                            <label htmlFor="uname">Username</label>
                            <input required name="uname" type="text" id="uname" placeholder="username" onChange={handleChange}/>
                        </div>
                        <div>
                            <label htmlFor="pw">Password</label>
                            <input required name="pw" type="password" id="pw" placeholder="password" onChange={handleChange}/>
                        </div>    
                    </form>
                </div>
            </div>
        </div>
    )
}