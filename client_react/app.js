
//----------------------------------------------------------------------------------------------------------------------

function Grid( { grid } ) {
   let prevDateD = new Date( grid[ 0 ][ 0 ] )          // grid[0][0] is interpreted as UTC0 time zone
   prevDateD.setUTCDate( prevDateD.getUTCDate() - 1 )   // initialize to day immediately preceding grid[0][0] so no date gaps on first iteration
   const days = [ ]

   for ( let i = 0; i < grid.length; i++ ) {          // all operations on dates should be in UTC
      let [ dateS, taskA ] = grid[ i ]
      let dateD = new Date( dateS )

      if ( dateD - prevDateD > 24 * 60 * 60 * 1000 ) {    // subtract returns difference in milliseconds
         prevDateD.setUTCDate( prevDateD.getUTCDate() + 1 )   // gap starts on day after previous day emitted

         for ( const d = prevDateD; d < dateD; d.setUTCDate( d.getUTCDate() + 1 ) ) {
            const dS = d.toISOString().slice( 0, 10 )
            days.push( <Day key={ dS } date={ dS } taskArray={ [ ] } /> )
         }
      }

      days.push( <Day key={ dateS } date={ dateS } taskArray={ taskA } /> )
      prevDateD = dateD
   }


   return (
      < >
         { [ days ] }
         <DateAdd lastDate={ grid.slice( -1 )[ 0 ][ 0 ] } />
      </>
   )
}

//----------------------------------------------------------------------------------------------------------------------

function Day( { date, taskArray } ) {
   let idArray = []
   taskArray.map(task => idArray.push(task.id))
   console.log(idArray)

   let className = "date"
   if ( taskArray.length == 0 ) className += " empty"

   return (
      <div id={ date } className="day">
         <div className={ className }>{ date }</div>
         { taskArray.map( task => <Task key={ task.id } taskData={ task } /> ) }
         <TaskAdd idArray={idArray} dateString={date}/>
      </div>
   )
}

//----------------------------------------------------------------------------------------------------------------------

function DateAdd( { lastDate } ) {

   let nextDate = new Date( lastDate )
   nextDate.setUTCDate( nextDate.getUTCDate() + 1 )

   const handleDateAdd = () => {
      fetch( `http://localhost:4567/date/${ nextDate.toISOString().slice( 0, 10 ) }`, {
               'method': 'post',
               'body': '[]',
               'headers': {
               'Content-Type': 'application/json'
               }
            })
      // window.location.reload()
   }

   return <div className="date date-add" onClick={ handleDateAdd }>+</div>
}

//----------------------------------------------------------------------------------------------------------------------

function Task( { taskData } ) {

   const dialogRef = React.useRef()

    const handleClick = (event) => {

      dialogRef.current.showModal()
   }

   return (
      <>
         <div data-task_id={ taskData.id } className={ "task " + taskData.status } onDoubleClick={ handleClick } >
            { taskData.title }
         </div>
         <dialog id={`taskDetailDialog-${taskData.id}`} ref={dialogRef}>
            <h4>{taskData.title}</h4>
            <div>
               <div>
                  Status:
                  <div>
                     <input id={`toDoCheck-${taskData.status}`} defaultChecked={taskData.status === "todo"} type="checkbox"/>
                     <label htmlFor={`toDoCheck-${taskData.status}`}>to do</label>
                  </div>
                  <div>
                     <input id={`doingCheck-${taskData.status}`} defaultChecked={taskData.status === "doing"} type="checkbox"/>
                     <label htmlFor={`doingCheck-${taskData.status}`}>doing</label>
                  </div>
                  <div>
                     <input id={`doneCheck-${taskData.status}`} defaultChecked={taskData.status === "done"} type="checkbox"/>
                     <label htmlFor={`doneCheck-${taskData.status}`}>done</label>
                  </div>
               </div>
            </div>
            <div>
               Contents: {taskData.contents}
            </div><div>
               Type: {taskData.type}
            </div>
         </dialog>
      </>
   )

}

//----------------------------------------------------------------------------------------------------------------------

function TaskAdd( { idArray, dateString } ) {

   const dialogRef = React.useRef()



   const [ formData, setFormData ] = React.useState({
      'title': '',
      'type':  '',
      'status':   'toDo',
      'contents': '',
    })


   const handleChange = ( event ) => {

      setFormData({
         ...formData,
         [ event.target.name ]: event.target.value
      })
   }

   const handleSubmit = async ( event ) => {
      event.preventDefault()

      if ( idArray.length === 0 ) {
         await fetch( `http://localhost:4567/date/${ dateString }`, {
            'method': 'post',
            'body': '[]',
            'headers': {
            'Content-Type': 'application/json'
            }
         })
      }

      const data = {
         'date': dateString,
         'task_list': idArray,
         'new': formData,
         'position': 0
      }

      console.log( data )

      const fetchConfig = {
         'method': 'post',
         'body': JSON.stringify( data ),
         'headers': {
            'Content-Type': 'application/json'
         }
      }

      fetch( 'http://localhost:4567/task', fetchConfig )

      // window.location.reload()
   }


   const handleClick = (event) => {
      // console.log(dateString)

      // const modalDialog = document.getElementById("taskAddDialog")

      if (event.target.id === "modalOpenButton") {
         dialogRef.current.showModal()
      } else if (event.target.id === "modalCloseButton") {
         dialogRef.current.close()
      }
   }

   return (
      <>
         <div id="modalOpenButton" className="task task-add" onClick={handleClick}>+</div>


         <dialog id="taskAddDialog" ref={dialogRef}>
            <button id="modalCloseButton" onClick={handleClick}>close</button>
            <form method="dialog" onSubmit={handleSubmit}>
               <section>
                  <label htmlFor="title">Title</label>
                  <input required id="title" name="title" onChange={handleChange}/>
               </section>
               <section>
                  <label htmlFor="type">Type</label>
                  <input required id="type" name="type" onChange={handleChange}/>
               </section>
               <section>
                  <label htmlFor="status">Status</label>
                  <input required id="status" name="status" onChange={handleChange}/>
               </section>
               <section>
                  <label htmlFor="contents">Contents</label>
                  <input required id="contents" name="contents" onChange={handleChange}/>
               </section>
               <button onClick={handleSubmit}>Submit</button>
            </form>
         </dialog>

      </>
   )
}

//----------------------------------------------------------------------------------------------------------------------

function TaskDetailModal( { show, onClose, taskData } ) {

    if ( ! show ) return null

    return (
        <div className="modal">
            <div className="modal-content">
                <div className="modal-header">
                    <h4 className="modal-title">{ taskData.title }</h4>
                </div>
                <div className="modal-body">
                    Status: { taskData.status }
                    <br/>
                    Contents: { taskData.contents }
                </div>
                <div className="modal-footer">
                    <button className="modal-button" onClick={ onClose }>Close</button>
                </div>
            </div>
        </div>
    )
}

//----------------------------------------------------------------------------------------------------------------------

const container = document.getElementById( "grid" )
const root = ReactDOM.createRoot( container )
root.render( <Grid grid={ grid }/> )
